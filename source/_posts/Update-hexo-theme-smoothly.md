---
title: Update hexo-theme-NexT smoothly
copyright: right
mathjax: true
top: false
layout: true
date: 2019-09-06 03:11:11
categories: 折腾笔记
description: 快速更新hexo主题NexT
tags:
	- Hexo
	- NexT
id: update-hexo-theme-smoothly
---

主题更新一直是个大问题，自己修改了主题的配置文件之后，就不能直接`git pull`来更新主题文件了，需要手动merge主题设置，太麻烦了，其实NexT官方给的文档里有写的，然而我现在才发现，还是要多看看文档啊。这是利用了Hexo3.0的特性。

### 利用Hexo的方法

在Hexo-site的配置文件里写入自己主题需要的配置

1. 添加一行`theme_config:`
2. 把你需要的配置都复制到`theme_config:`下
3. 注意要缩进，把所有主题配置都缩进两个空格

如果存在`hexo/source/_data/next.yml`这个文件，请删掉它，如果有这个文件，将是使用NexT的方法来保存配置文件。

### 利用NexT的方法

在`hexo/source`文件夹中新建文件夹`_data`，创建`next.yml`

如果你把配置文件的`override`设置为`true`，你只需要把所有的NexT配置复制到`next.yml`中即可；如果你把配置文件的`override`设置为`false`，你需要把你所需要的hexo配置和主题NexT的配置复制到`next.yml`内。

官方是推荐使用第一种方法的。

### 事后

调整好之后，请执行`hexo clean`把public文件夹清一下，再重新生成页面、部署等等。

[官方文档](https://theme-next.org/docs/getting-started/data-files)