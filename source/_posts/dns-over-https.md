---
title: DNS-over-HTTPS
copyright: right
mathjax: true
top: false
layout: true
date: 2019-08-07 19:32:43
categories: 折腾笔记
description: 通过DoH来防止DNS污染
tags:
	- dns
	- DoH
id: dns-over-https
---

## DNS

一个帮我们将记住对应域名所指向IP地址的服务器，的确很方便，我们不用去记住一些晦涩难懂的IP地址。

## DNS劫持

通过劫持DNS来修改某些域名的解析记录，从而使访问该域名的人得到错误的IP，导致我们无法访问一些网站，当然有些网站不应该由DNS背锅，那是有别的一些原因导致的。

## DNS污染

让用户得到虚拟目标主机的IP，因为DNS没有认证机制，通常基于UDP，速度的确快，但是会被篡改。

## 解决方法

### 解决DNS劫持

将DNS换成公共DNS就ojbk。

### 解决DNS污染

改Hosts、加密DNS请求。

### 如何加密DNS请求呢？

#### [dnscrypt](https://dnscrypt.info/)

我没用过，不会用QAQ。

#### DoT

DNS-over-TLS，TLS(Transport Layer Security)，通过853端口，但是有个很神奇的东西，当你使用853端口的时候，就有人知道你在用DoT，当然，你也可以改端口号。

#### DoH

DNS-over-HTTPS，通过443端口，与HTTPS同端口(废话)，我正在使用，用了之后能访问ddg，dde(deepin desktop environment)+ddg更配噢，差个ddf♂(雾

## 公共DNS

| Name           | DNS1      | DNS2      |
| -------------- | --------- | :-------- |
| Cloudflare DNS | 1.1.1.1   | 1.0.0.1   |
| ALIDNS         | 223.5.5.5 | 223.6.6.6 |
| google DNS     | 8.8.8.8   | 8.8.4.4   |

Cloudflare DNS是Cloudflare与APNIC合作新推出的公共DNS。

> 原来，Cloudflare 并不拥有 1.1.1.1 和 1.0.0.1 这两个 IP，而是从 APNIC 那里得到了使用权。APNIC  全称 Asia-Pacific Network Information  Centre，亚太网络信息中心，是世界五大区域互联网注册机构之一。它管理亚洲以及太平洋地区的互联网资源，其中我们最熟悉的就是 IP  地址了。APNIC 作为分配 IP 地址的官方，持有几个靓号自然是合情合理了。
>
> 靓号虽好，可不是谁都能用的。如果不是数字简单，方便记忆，1.1.1.1  其实只是几十亿 IPv4 中的普通一个。它也不属于标准规定的保留地址（配置过路由器的同学，多少遇到过 192.168.x.x  这样的为内网或其他用途保留的号段）。但就是因为它太好记，并且一直没有被使用，所以很多网络运维测试需要随便填一个 IP 时就顺手填了个  1.1.1.1。甚至有些黑客也用 1.1.1.1 来测试其僵尸网络。于是，1.1.1.1  就被来自世界各地的垃圾信息洪流淹没了，几乎是一场永不停息的 DDoS 攻击。
>
> 这个时候，主角 Cloudflare  登场了。Cloudflare 的主营业务就是网络安全，应对 DDoS 更是看家本领。所以他们主动找上了  APNIC，提出了自己建立一个隐私优先、速度极致的 DNS 服务的设想。APNIC 十分感动，同意了 Cloudflare  的提议。从此，Cloudflare 要用自己的网络来承受原本指向 1.1.1.1 的“垃圾”信息，也换得了在靓号上运行 DNS  解析服务器的机会。
>
> 来自[用上 ip 靓号1.1.1.1，Cloudflare 花了多少钱？](https://zhuanlan.zhihu.com/p/36169153)
>
> This work is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

## 如何在Firefox上使用DoH

1. 在Firefox的地址栏输入 ``about:config``，并确认了解此风险，不了解就不使用DoH呗
2. 搜索``network.trr``
3. 将``network.trr.mode``修改为2，意为首选DoH，实在不行就用平常的DNS查询方法
4. ``network.ttr.uri``是``https://mozilla.cloudflare-dns.com/dns-query``就OK了，当然你也可以改成其他支持DoH的服务器网址。支持的好像不多，感觉这个已经比较快了。
5. 修改``network.trr.bootstrapAddress``为一个后备的DNS，我就改为了1.0.0.1，因为我这1.1.1.1是不能用的。
6. 进入网址[Browsing Experience Security Check](https://www.cloudflare.com/ssl/encrypted-sni/),cloudflare给的一个检查DNS服务是否走cloudflare的，Check My Browser后，看到最右边是Secure DNS就OK了。

## 使用体验

访问国内的网站有点慢，国外的网站就感觉快了一丢丢，github不会因为DNS污染而访问不了了，我自己也难得一个个地去改Hosts了，~~重要的是能上ddg了~~，再不不需要百毒了，注意，还是用不了谷歌的，别想一些有的没的。

ddg的话，自己找ip改改hosts就可以用了

毕竟Cloudflare在国内没有节点，用起来会比较慢，在国内的话，可以选择使用[GeekDNS](https://www.233py.com/)，自己选择一个靠近自己的服务器即可，香港的dns已经被墙了，本来想用dnscrypt-proxy的，总是搞不定，系统总是把dns改为路由器给的dns，这个解决了，又出现忽然停止服务的情况，我都懵了，不想折腾了，这个系统还是需要重装的

## DoH查询格式

https://dns.containerpi.com/dns-query?name=dns.containerpi.com&type=A

