---
title: '[POJ1475] Pushing Boxes 题解报告'
copyright: right
mathjax: true
top: false
categories: 题解报告
description: POJ1475题解报告
tags: BFS
id: POJ1475
abbrlink: 1779076171
date: 2019-04-06 15:19:34
---

[传送门](http://poj.org/problem?id=1475)

## 题意

推箱子，输出路径，先要求整个的路径最短，如果有多个最短路径，选推箱子的次数最少的。没有路径的输出Impossible。

## 题解

很明显，箱子要跑bfs，但跑bfs的时候要判断移动是否合法，箱子移动后，人必在箱子移动前的位置，而人要能到达箱子移动的反方向的那个格子上。

### 例子1

``` cpp
############
#.......TB##
#..........#
#......S...#
############
```

像这种的人要能到达B的右边才能推动箱子。

所以在跑箱子bfs的时候，要判断人能不能到达推动箱子的那个格子上。

明显是双bfs。

### 例子2

``` cpp
############
#T##########
#..B.......#
#.#######..#
#.....S....#
############
```

转移到

``` cpp
############
#T##########
#BS........#
#.#######..#
#..........#
############
```

人就要去到箱子的另外一边推。可能一开始想到这个感觉会有点棘手，打起来就也就那样打。

所以箱子的bfs的状态上就存箱子的座标、人的座标和答案，每次按题意更新答案。

``` cpp
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <string>
#include <bitset>
#include <vector>
#include <cmath>
#include <ctime>
#include <queue>
#include <stack>
#include <map>
#include <set>
using namespace std;
const char box[]={'N','S','W','E'};
const char peo[]={'n','s','w','e'};
const int   dx[]={-1,1,0,0};
const int   dy[]={0,0,-1,1};
int n,m,num;
char mmp[25][25];
string tmp;
struct P{
    int x,y,px,py;
    string ans;
};
bool valid(int x,int y){
    return x>0&&x<=n&&y>0&&y<=m&&mmp[x][y]!='#';
}
bool bfs2(P p1,P p2){
    tmp="";
    P st;
    st.x=p1.px;
    st.y=p1.py;
    st.ans="";
    queue<P>q;
    q.push(st);
    bool v[26][26];
    memset(v,0,sizeof v);
    while(q.size()){
        P now=q.front(),nxt;
        q.pop();
        if(now.x==p1.x&&now.y==p1.y){
            tmp=now.ans;
            return 1;
        }
        for(int i = 0; i<4;i++){
            nxt = now;
            nxt.x = now.x + dx[i];
            nxt.y = now.y + dy[i];
            if (!valid(nxt.x, nxt.y)) continue;
            if (nxt.x == p2.x && nxt.y == p2.y) continue;
            if (v[nxt.x][nxt.y]) continue;
            v[nxt.x][nxt.y] = 1;
            nxt.ans = now.ans + peo[i];
            q.push(nxt);
        }
    }
    return 0;
}
string bfs1(){
    P st;
    st.x=st.y=st.px=st.py=-1;
    st.ans="";
    for(int i=1;i<=n&&(st.x==-1||st.px==-1);i++){
        for(int j=1;j<=m&&(st.x==-1||st.px==-1);j++){
            if(mmp[i][j]=='B'){
                st.x=i;
                st.y=j;
                mmp[i][j]='.';
            }else if(mmp[i][j]=='S'){
                st.px=i;
                st.py=j;
                mmp[i][j]='.';
            }
        }
    }
    queue<P>q;
    q.push(st);
    bool v[25][25][4];
    memset(v,0,sizeof v);
    string ans = "Impossible.";
    unsigned int cntans=0x3f3f3f3f,cnt=0x3f3f3f3f;
    while(q.size()){
        P prv,now=q.front(),nxt;
        q.pop();
        if(mmp[now.x][now.y]=='T'){
            unsigned int cntnow=0;
            unsigned int len = now.ans.length();
            for(unsigned int i =0;i<len;i++){
                if(now.ans[i]>='A'&&now.ans[i]<='Z')cntnow++;
            }
            if(cntnow<cntans||(cntnow==cntans&&len<cnt)){
                ans = now.ans;
                cntans=cntnow;
                cnt=len;
            }
            continue;
        }
        for(int i=0;i<4;i++){
            nxt=now;
            nxt.x = now.x + dx[i];
            nxt.y = now.y + dy[i];
            if(!valid(nxt.x,nxt.y))continue;
            if(v[nxt.x][nxt.y][i])continue;
            prv = now;
            if(i==3)prv.y=now.y-1;
            else if(i==2)prv.y=now.y+1;
            else if(i==1)prv.x=now.x-1;
            else prv.x=now.x+1;
            if(!bfs2(prv,now))continue;
            v[nxt.x][nxt.y][i]=1;
            nxt.ans=now.ans+tmp+box[i];
            nxt.px=now.x;
            nxt.py=now.y;
            q.push(nxt);
        }
    }
    return ans;
}
void work(){
    for(int i=1;i<=n;i++)cin>>(mmp[i]+1);
    cout<<"Maze #"<<++num<<endl<<bfs1()<<endl<<endl;
}
int main(){
    //freopen("in.txt","r",stdin);
    //freopen("out.txt","w",stdout);
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    num=0;
    while(cin>>n>>m&&n&&m){
        work();
    }
    return 0;
}
```

刷[蓝书](https://item.jd.com/20111313.html)真虐心，基础不熟练啊，加油加油。