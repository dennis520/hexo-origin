---
title: 墙裂推荐Firefox
copyright: right
mathjax: true
top: false
layout: true
date: 2019-08-07 21:28:19
categories: 安全
description: 需要Firefox来制衡Chromium
tags: 
	- browser
	- privacy
id: recommend-using-firefox
---

Firefox的速度还是比不上chrome，这没得说，内存比chrome占用少，但是Firefox在隐私这方面比chrome好，而且现在能设置DoH了，Firefox68能在网络设置界面设置DoH了，用着还算舒服，还有不用神奇的上网方式就能用官方的方式同步标签、装插件，很舒服。~~github上有破解版的插件装在chrome上就能访问google，使用官方的同步。~~

chrome是基于chromium内核的浏览器，而且现在越来越多浏览器使用chromium内核，基于chromium内核的浏览器的份额越来越大，巨硬的edge也开始用chromium内核了，一家独大的话，标准的制定就很难起作用了，因为只需要对chromium适配就ojbk，chromium成为了标准，万维网联盟(W3C)会被架空了？导致更多人去使用基于chromium内核的浏览器，第二个IE?而且google公司就能拥有更多的用户，更多用户数据，隐私问题lei了，毕竟google是个广告公司，通过打标签给你推送特定的广告自己能获得更多的收入，也是个正常的行为，毕竟谷歌大部分的收入都来自广告。

~~你甚至可以使用Tor-browser~~

