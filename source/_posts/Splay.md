---
title: splay
copyright: right
mathjax: true
top: false
categories: 学习笔记
description: 伸展树学习笔记
tags:
  - data structure
  - splay
id: splay
date: 2019-04-23 12:23:34
---
## 来感受Splay的魅力8 ##

话说这个树也是[Tarjan](https://en.wikipedia.org/wiki/Robert_Tarjan)巨巨和其他巨巨发明的，凸轮已经在Tarjan巨巨的支配中了，然而其他领域也在他的支配中。

### 性质 ###

可以说它也是棵自平衡的二叉查找树，但是不是十分平衡，存在有O(n)的操作情况，但均摊的操作效率被巨巨们证明为O(logn)的。Splay是通过伸展操作调整自身的，使自己变得比较平衡。而Splay规定每访问节点，就把它伸展(Splay)到根节点(方便查找?)。

### 操作 ###

- 插入

- 删除

- 查找

- 区间翻转

- (未完待续)



### 需要维护的东西 ###

```cpp
struct Node;
struct Node{
    struct Node* ch[2]，fa;//左孩子、右孩子、父亲
    int inv;//翻转操作的lazy标记
    int val;
    int size;//当前树的大小
}
//当然,这也可以用多个数组维护
```

#### 旋转操作(小天才用符号画图) ####

```cpp
/*
       a           右旋             b
     /   \         ->             / \
    b     c                      d   a
   / \             <-               / \
  d   e            左旋             e   c
*/
```

看起来，这个旋转操作一点意思都没有嘛，感觉没个卵用，又不能减少树高。

等等党：等等，单旋看起来莫得什么用，双旋来帮锤。

#### 双旋操作(小天才用符号画图) ####

#####  一字型 #####

```cpp
/*
        a           d to a             d
       / \           ->               / \
      b   c                          f   a
     / \             <-                 / \
    d   e          a to d              b   c
   / \                                / \
  f   g                              g   e
*///这样旋是会被卡的,应先旋父亲再旋自己
```


```cpp
/*                          优化  旧链 a-b-d-g d-a-b-g
                                 d to a
        a           b to a             b                    d
       / \           ->              /   \                /   \
      b   c                         d     a    d to a    f     b
     / \                           / \   / \      ->          / \
    d   e                         f   g e   c                g   a
   / \                                                          / \
  f   g                                                        e   c
*/
```

(不还是对高度莫得影响么)

等等党：看下面的操作，很直观的。

#####  之字型 #####

```cpp
/*
         a           e to a               e
       /   \           ->               /   \
      b     c                          b     a
     / \                              / \   / \
    d   e                            d   f g   c
       / \
      f   g
*/
//左右对称，自行脑补，这个不能双箭头，我也很无奈，但是真的有肉眼能看到的压缩啊
```


####  区间操作 ####

有Splay操作(伸展操作)的支持，区间操作就很简单的啦。

比如你想翻转一个$[1,a]$的区间，你只要找到比$a$大的一个点，让它旋到根节点，那么根节点的左子树就是[1,a]的区间了。

