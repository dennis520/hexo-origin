---
title: SAM
copyright: right
mathjax: true
top: false
categories: 学习笔记
description: 后缀自动机学习笔记
tags:
  - data structure
  - SAM
id: SAM
abbrlink: 1890847378
date: 2019-05-07 17:15:20
---

# Suffix automaton

后缀自动机，由一个parent树和一个DAG构成。DAG的边连接着状态转移的两点，点就是状态。

每个节点就是一个endpos等价类，每条parent树的边代表的是endpos等价类包含的关系。DAG就是状态转移的过程。

解析一下代码，个人感觉讲得不太好，可以移步看一下[史上最通俗的后缀自动机详解](https://www.luogu.org/blog/Kesdiael3/hou-zhui-zi-dong-ji-yang-xie)和[oi-wiki上的SAM](https://oi-wiki.org/string/sam/)，了解一下endpos类等等的定义。

[luogu3804](https://www.luogu.org/problemnew/show/P3804)

```cpp
#include <algorithm>
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <string>
#include <cmath>
#include <ctime>
#include <queue>
#include <map>
#include <set>
/*
#include <unordered_map>
#include <unordered_set>
*/
using namespace std;
const int N = 1000010;
int c[N],dfn[N];
struct SAM{
    static const int N = 1000010;
    static const int CHAR_SET_SIZE=26;
    //这里的<<1，是因为后缀自动机的状态的个数是线性的，必定小于2*n，我就不证了(我不会证)。
    //ch是每个节点转移的下一个状态，fa是parent树的父节点，len是当前状态的串的长度。siz是每个节点的对应的串出现的次数
    int ch[N<<1][CHAR_SET_SIZE],fa[N<<1],len[N<<1],siz[N<<1];
    int tot,last;
    int newnode(){
        ++tot;
        memset(ch[tot],0,sizeof ch[tot]);
        fa[tot]=0;
        return tot;
    }
    //初始化，把最开始的空状态给搞出来
    void init(){
        tot=1;
        last=1;//记录上次的节点，下次在这后面增量构造SAM
        len[1]=0;
        memset(ch[1],0,sizeof ch[1]);
        memset(siz,0,sizeof siz);
    }
    void add(int x){
        int pos = last,newpos=newnode();
        last = newpos;
        siz[newpos]=1;//后面需要这个来计算每个节点的串出现的次数
        len[newpos]=len[pos]+1;
        //更新父节点的等价类，让没有转移到当前字符的节点转移到当前字符，
        while(pos&&!ch[pos][x]){ch[pos][x]=newpos;pos=fa[pos];}
     	//如果！pos就是根本没出现过这个字符，那么这个等价类的就是新的，连到根节点。
        if(!pos)fa[newpos]=1;
        else{
		//如果有父节点连接到x这个状态，我们对此进行判断
            int oldpos=ch[pos][x];
            //如果这个父节点的儿子的长度跟父节点的长度相差1，那么我们新的这个等价类的父节点就是这个父节点的儿子了，因为我们必定比父节点的儿子要更长，肯定是再被划分出来的。
            if(len[oldpos]==len[pos]+1)fa[newpos]=oldpos;
            else{
            //否则，我们就创建一个新的状态的长度是父节点的长度+1的。让祖先们的x转移为原父亲x转移的都改为对新状态的转移，让原父亲x转移的状态改为对新状态的转移。
                int anp=newnode();
                memcpy(ch[anp],ch[oldpos],sizeof ch[anp]);
                fa[anp]=fa[oldpos];
                len[anp]=len[pos]+1;
                fa[oldpos]=fa[newpos]=anp;
                while(pos&&ch[pos][x]==oldpos){
                    ch[pos][x]=anp;
                    pos=fa[pos];
                }
            }
        }   
    }
    long long solve(){
        //暴力更新每个节点的字符串出现的次数，然后对出现次数>1的更新答案。
        long long ans=0;
        memset(c,0,sizeof c);
        for(int i=1;i<=tot;i++)c[len[i]]++;
        for(int i=1;i<=tot;i++)c[i]+=c[i-1];
        for(int i=1;i<=tot;i++)dfn[c[len[i]]--]=i;
        for(int i=tot;i>=1;--i){
            int now = dfn[i];
            siz[fa[now]]+=siz[now];
            if(siz[now]>1)ans=max(ans,1ll*len[now]*siz[now]);
        }    
        return ans;
    }
}sam;
char s[1000010];
int main(){
    if(fopen("in.txt","r")){
        freopen("in.txt","r",stdin);
        freopen("out.txt","w",stdout);
    }
    sam.init();
    scanf("%s",s);
    int len = strlen(s);
    for(int i=0;i<len;i++)sam.add(s[i]-'a');
    printf("%lld\n",sam.solve());
    return 0;
}

```

