---
title: FTC Warning
copyright: right
mathjax: true
top: false
tags: trick
categories: 错误记录
description: 常见错误
id: FTC Warning
abbrlink: 3916653506
date: 2019-03-09 14:57:50
---

## FXXK the code

### ACM

- 检查全局变量和局部变量
- 改用int就不要用char，该用long long的时候就不要用int
- switch一定要用break(不熟悉就别用)
- 跑线性逆元和阶乘的时候，从2开始跑。
- BFS要注意入队情况

### Markdown

- 在有数学表达式的markdown文件里面不要用星号，会被解析为斜体，\cdot(点乘)，\times(叉乘)，\div(除以)。
$$
\cdot(点乘)，\times(叉乘)，\div(除以)
$$

