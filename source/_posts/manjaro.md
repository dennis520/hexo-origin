---
title: manjaro折腾记
copyright: right
mathjax: true
top: false
categories: 折腾笔记
description: manjaro心路历程
tags:
  - manjaro
  - linux
id: manjaro1
abbrlink: 345148806
date: 2019-03-12 08:42:55
---

## 过度过程

ubuntu->deepin->manjaro-deepin->manjaro kde->manjaro-deepin

## 在deepin过度到manjaro-deepin ##

deepin的dde有丶好用~~（其实是不想 去重新熟悉一个新的环境，例如KDE）~~真香。

## 在windows下烧U盘

 要用[refus](https://rufus.ie/)，要用DD模式烧。用Universal USB Installer烧的话，会无法引导。

学校的drcom会占用XXshellXXX.dll，忘记具体名称了，会导致refus使用不了，关了就完事

## 源

### 国内源

```shell
sudo pacman-mirrors -i -c China  //改为国内源,在n弹出的框中选一个就好
sudo pacman -Syy  //更新数据源
```

### archlinuxcn

修改/etc/pacman.conf，在最后添加

下面选一个就行了

```shell
[archlinuxcn]
Server = https://mirrors.tuna.tsinghua.edu.cn/archlinuxcn/$arch  //添加清华源
```

```shell
[archlinuxcn]
Server = https://mirrors.ustc.edu.cn/archlinuxcn/$arch //添加中科大源
```

之后要添加PGP签名

```shell
sudo pacman -S archlinuxcn-keyring
```

刷新缓存

```shell
sudo pacman -Syy
```

我校园网用清华源能跑满带宽，中科大的不行，中科大的更新速度略快于清华。

## 安装搜狗拼音

```shell
sudo pacman -S fcitx-im
sudo pacman -S fcitx-configtool
sudo pacman -S fcitx-sogoupinyin
```

修改配置文件~/.xprofile

```shell
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=”@im=fcitx”
```

## 其他很多都能在manjaro的添加/删除软件里面找

gugugu

## KDE

### baloo_file_extractor

就是这个家伙，总把我的CPU跑12%，用久之后占用3G的共享内存

```bash
su
mv /usr/bin/baloo_file_extractor /usr/bin/baloo_file_extractor.orig
ln -s /bin/true /usr/bin/baloo_file_extractor
```

这就完事

## ibus-rime

fcitx后台也好像挺费CPU的，所以也换到ibus了，rime根据github上的来配置简体中文即可（要看看文档了）

---

# updata：2019-04-06

我又从kde跳回deepin了，kde的设置太多了

---

# update : 2019-05-01

尝试了三个flagship版(kde,xfce,gnome)的manjaro和15.10的deepin，本来想用回deepinos的，第一次装wechat什么的都可以的，第二次装就说依赖出问题了，不愧是基于debian的啊，然后又装回来了，晕死。不愧是基于arch的manjaro，xjb装就可以，很少考虑依赖的问题。

## ibus

```shell
sudo pacman -S ibus ibus-rime
```

修改配置文件~/.xprofile

添加下面几行

```shell
export GTK_IM_MODULE=ibus
export QT_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
ibus-daemon -d -x
```

修改配置文件~/.bashrc

添加下面几行

```shell
export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus
```

感觉操作冗余了，dalao们教一教啊。

## oh-my-zsh

### 安装

下面两行选一个就行了，第一行为curl，第二行为wget，没装的自己装一下就行。

```shell
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
```

.zshrc

```
ZSH_THEME="agnoster"
plugins=(git autojump zsh-autosuggestions zsh-syntax-highlighting archlinux)
```