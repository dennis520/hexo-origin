---
title: WebUI-Aria2
copyright: right
mathjax: true
top: false
layout: true
date: 2019-08-18 00:13:36
categories: 折腾笔记
description: 在自己的服务器上折腾一个离线下载服务
tags: 
	- linux
	- aria2
id: webui-aria2
---

[github repo](https://github.com/ziahamza/webui-aria2)

### 前端用法

官方给出的用法

- 下载整个仓库，然后从 `docs` 文件夹中打开index.html
- 通过访问 https://ziahamza.github.io/webui-aria2 来下载文件
- 在项目文件夹内通过`node node-server.js`来创建一个服务

### 后端用法

通过以下命令启动aria2

```
aria2c --enable-rpc --rpc-listen-all
```

因为默认端口为6800，如果是部署在云端服务器的话，可以通过配置文件更改端口，或者在服务器商开启特定端口，使得服务正常使用。

### 将aria2设置为开机自启

创建/修改文件`/lib/systemd/system/aria2.service`

```service
[Unit]
Description= aria2
After=network.target
 
[Service]
ExecStart=/usr/bin/aria2c --conf-path=***************
ExecStop=/bin/kill $MAINPID
RestartSec=always
 
[Install]
WantedBy=multi-user.target
```

```
systemctl enable aria2.service
systemctl start aria2.service
```

### 注意

如果你是通过https访问的话，aria2的rpc服务可能也需要开启SSL/TLS加密，我是直接用网站的https的证书就ojbk了。

### aria2 bt下载加速

在`aria2.conf`的目录内运行这个sh文件

```
#!/bin/bash
killall aria2c
list=`wget -qO- https://raw.githubusercontent.com/ngosang/trackerslist/master/trackers_all.txt|awk NF|sed ":a;N;s/\n/,/g;ta"`
if [ -z "`grep "bt-tracker" ./aria2.conf`" ]; then
    sed -i '$a bt-tracker='${list} /root/.aria2/aria2.conf
    echo add......
else
    sed -i "s@bt-tracker.*@bt-tracker=$list@g" ./aria2.conf
    echo update......
fi
```

如果无法运行请给它改为可运行文件

```
chmod +x ./trackers-list-aria2.sh
```

