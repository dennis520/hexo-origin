---
title: A_star不详解
copyright: right
mathjax: true
top: false
categories: 学习笔记
description: A*算法简略笔记
tags:
  - ACM
  - algorithm
  - A_star
id: A-star
date: 2019-04-23 01:44:26
---

### 前置知识

- 优先队列bfs
- 单源最短路算法

### 场景

当我们选择一条最短的路的时候，我们总是走估计路的距离最短的路，我们中途发现道路堵塞的时候，觉得走其他路可能会更快，而不是走直线。这其实跟A_star算法很像啦(不敢说一样啊)。

### 算法原理

A_star需要一个估价函数，估计当前状态到目标的花费，然后每次根据当前花费+估计的花费进行优先队列bfs，保证每次取出的是当前花费+估计的花费最小的，然后去扩展下一个状态，就是每次保证当前花费+估计的花费最小的，而且我们要保证估价出来的花费要小于等于实际花费，不然就会跑错，如果到达终点了，那个花费必然是最小的。

### 来道题目吧

[POJ2449-Remmarguts' Date](http://poj.org/problem?id=2449)求k短路。

按照A_star算法，第k次访问终点的时候就是K短路的花费啦。估价函数也很容易设计，一个点到终点的估价跑个单源最短路就OK了。

### 代码

``` cpp
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <string>
#include <cmath>
#include <ctime>
#include <queue>
#include <stack>
#include <map>
#include <set>
using namespace std;
const int N = 100010;
int tot,n,m;
int Head[1010],nxt[N<<1],ver[N<<1],w[N<<1];
int d[1010];
void add(int u,int v,int d) {
    ver[tot]=v;
    w[tot]=d;
    nxt[tot]=Head[u];
    Head[u]=tot++;
}
bool v[1010];
void dijk(int s) {
    priority_queue<pair<int,int>,vector<pair<int,int> >,greater<pair<int,int> > >q;
    memset(d,0x3f,sizeof d);
    memset(v,0,sizeof v);
    q.push(make_pair(0,s));
    d[s]=0;
    while(q.size()) {
        pair<int,int> tmp = q.top();
        q.pop();
        int u = tmp.second;
        if(v[u])continue;
        v[u]=1;
        for(int i=Head[u],v; i; i=nxt[i]) {
            if(i&1)continue;
            v=ver[i];
            if(d[v]>d[u]+w[i]){
                d[v]=d[u]+w[i];
                q.push(make_pair(d[v],v));
            }
        }
    }
}
int cnt[1010];
void kth() {
    int s,t,k;
    scanf("%d%d%d",&s,&t,&k);
    if(s==t)k++;
    dijk(t);
    memset(cnt,0,sizeof cnt);
    priority_queue<pair<int,int>,vector<pair<int,int> >,greater<pair<int,int> > >q;
    q.push(make_pair(d[s],s));
    while(q.size()) {
        pair<int,int> tmp = q.top();
        q.pop();
        int u = tmp.second;
        ++cnt[u];
        if(cnt[t]==k) {
            printf("%d\n",tmp.first);
            return ;
        }
        for(int i=Head[u],v; i; i=nxt[i]) {
            if(i&1){
                v=ver[i];
                if(cnt[v]!=k)q.push(make_pair(tmp.first-d[u]+w[i]+d[v],v));
            }
        }
    }
    puts("-1");
}
int main() {
    //freopen("in.txt","r",stdin);
    //freopen("out.txt","w",stdout);
    scanf("%d%d",&n,&m);
    memset(Head,0,sizeof Head);
    tot=1;
    for(int i=1,x,y,z; i<=m; i++) {
        scanf("%d%d%d",&x,&y,&z);
        add(x,y,z);
        add(y,x,z);
    }
    kth();
    return 0;
}
```

