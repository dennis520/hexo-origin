---
title: Palindrome automata
copyright: right
mathjax: true
top: false
layout: true
date: 2019-09-08 23:01:57
categories: 学习笔记
description: 回文自动机学习笔记
tags:
- data structure
- ACM
id: Palindrome-automata
---

### 简介

回文自动机是一个保存一个串中所有的回文串，从根节点到任一节点，所经过的边并起来就是一个回文串的一半，奇长度的与偶长度的根不一样，奇长度的回文串的第一条边所带表的字符只能算一次，也就是奇根到节点的边所表达的字符，其余情况都是算两次，构成一个回文串。

### 构成

两个根节点，奇根和偶根，还有其他节点，由代表某个字符边连接起来，还有fail指针连接回文串与这个回文串的后缀是回文串的最长串。

### 构造

像其他自动机一样，动态插入字符进行构造。

从上一次插入字符对应的节点，假如插入新字符能够构成回文串，那就插入，如果已经有节点了，就无需重复插入，如果查找不到，就通过fail指针去转移。

### 板子

[luogu3649](https://www.luogu.org/problem/P3649)

``` cpp
#include <algorithm>
#include <iostream>
#include <cassert>
#include <cstdlib>
#include <cstring>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <cmath>
#include <ctime>
#include <queue>
#include <stack>
#include <map>
#include <set>
/*
#include <unordered_map>
#include <unordered_set>
*/
using namespace std;
static const int N = 300010;
char s[N];
int size;
int ch[N][26], fail[N], len[N], tot;
int last;
int cnt[N];
int newnode(int l) {
    len[++tot] = l;
    memset(ch[tot], 0, sizeof ch[tot]);
    return tot;
}
int getfail(int p, int pos) {
    while (s[pos - len[p] - 1] != s[pos])p = fail[p];
    return p;
}
int main() {
    //freopen("in.txt","r",stdin);
    //freopen("out.txt","w",stdout);
    scanf("%s", s + 1);
    int length = strlen(s + 1);
    s[0] = -1, fail[0] = 1, last = 0;
    len[0] = 0, len[1] = -1, tot = 1;
    for (int i = 1, u, v; i <= length; i++) {
        s[i] -= 'a';
        u = getfail(last, i);
        if (!ch[u][s[i]]) {
            v = newnode(len[u] + 2);
            fail[v] = ch[getfail(fail[u], i)][s[i]];
            ch[u][s[i]] = v;
        }
        ++cnt[last = ch[u][s[i]]];
    }
    long long ans = 0;
    for (int i = tot; i; --i) {
        cnt[fail[i]] += cnt[i];
        ans = max(ans, 1ll * cnt[i] * len[i]);
    }
    printf("%lld\n", ans);
    return 0;
}
```

