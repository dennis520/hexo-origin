---
title: 查看nginx监听端口
copyright: right
categories: 学习笔记
description: nginx瞎折腾笔记
tags:
  - linux
  - web
  - nginx
id: nginx1
abbrlink: 154242604
date: 2018-03-22 14:49:47
top:
---

netstat -tlnup|grep nginx