---
title: Netlify
copyright: right
mathjax: true
top: false
layout: true
date: 2019-09-04 01:58:16
categories: 折腾笔记
description: 使用netlify自动部署静态网站
tags: 
	- github
	- Hexo
	- Netlify
id: netlify-blog
---

## 动机

- 想折腾一下
- githubpage还是留给项目页面用吧~~(你哪有什么项目)~~

## 使用方法

1. 登录注册netlify
2. 绑定你的github的仓库

然后它自己会部署，部署好之后，你之后就会有一个`.netlify.com`的网址，你可以通过这个访问你hexo生成的网站，~~这个域名会很丑，难以记忆，最好自己绑个域名，会好看许多。~~可以自己修改的，还是可以用的。

所以这个方法指建议有自己域名的人使用，有时候还感觉会反向加速，速度不如githubpage。

疯起来还真的啥都折腾。