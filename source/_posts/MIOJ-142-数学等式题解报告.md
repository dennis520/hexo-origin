---
title: MIOJ_142_数学等式题解报告
copyright: right
mathjax: true
top: false
categories: 题解报告
description: MIOJ142题解报告
tags: MITM
id: MIOJ142
abbrlink: 4040420115
date: 2019-04-08 23:46:41
---

## 题意

$$
a\times{x_1}^3+b\times{x_2}^3+c\times{x_3}^3=d\times{x_4}^3+e\times{x_5}^3
$$

给你这个式子，给你$abcde$ 这5个数，你找出满足这条式子的($x_1,x_2,x_3,x_4,x_5$)的组数。而且$x,a,b,c,d,e$均在$[-50,50]$的范围里面。

## 题解

无脑暴力$O(n^5)$是不行的。

优雅的暴力$O(n^3)$，先把左边的全部数值给暴力出来，再做右边的数，看看有没有数在左边的数值中出现过。我写了二分查找，好像也可以直接计数。

## 代码

``` cpp
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <string>
#include <cmath>
#include <ctime>
#include <queue>
#include <stack>
#include <map>
#include <set>
using namespace std;
long long num[1040000];
long long n3[120];
int main(){
    //freopen("in.txt","r",stdin);
    //freopen("out.txt","w",stdout);
    for(int i=-50;i<=50;i++){
        n3[i+50]=i*i*i;
    }
    long long a,b,c,d,e;
    int top=0;
    scanf("%lld%lld%lld%lld%lld",&a,&b,&c,&d,&e);
    for(int i=-50;i<=50;i++){
        if(i==0)continue;
        for(int j=-50;j<=50;j++){
            if(j==0)continue;
            for(int k=-50;k<=50;k++){
                if(k==0)continue;
                num[top]=(a*n3[i+50]+b*n3[j+50]+c*n3[k+50]);
                top++;
            }
        }
    }
    long long ans=0,tmp;
    sort(num,num+top);
    for(int i=-50;i<=50;i++){
        if(i==0)continue;
        for(int j=-50;j<=50;j++){
            if(j==0)continue;
            tmp=d*n3[i+50]+e*n3[j+50];
            ans+=upper_bound(num,num+top,tmp)-lower_bound(num,num+top,tmp);
        }
    }
    printf("%lld\n",ans);
    return 0;
}
```

开num数组的时候想着是$100*100*100$，编译器已经给warning了，然后跑了一下RE，后面才发现是$101*101*101$，我佛了。

## 附

这里有一个比较重要的思想：MITM(meet in the middle)

[阮行止聚聚的知乎](https://www.zhihu.com/people/ruan-xing-zhi/activities)

[阮行止聚聚的课件](https://ruanx.pw/post/MEET-IN-THE-MIDDLE.html)

