---
title: 各种数学推论
copyright: right
mathjax: true
top: false
categories: 学习笔记
description: 常用的数学公式，其实是自己很容易忘
tags: math
id: mathmix
abbrlink: 2855115949
date: 2019-03-17 21:50:37
---

## 极限

### $\lim \limits_{n \to inf} (1+\frac{x}{n})^n=e^x$

要证上式即是要证下式
$$
n\times ln(1+\frac{x}{n})=x
$$
令$k=\frac{1}{n}$，使得$n=\frac{1}{k}$
$$
\frac{ln(1+x\times k)}{k}=x
$$
下面左式来个洛必达$\frac{0}{0}$
$$
\frac{x}{1+x\times k}=x
$$
证毕



## 组合数

[转自zhaozhengcc](https://blog.csdn.net/bigtiao097/article/details/77242624)
$$
C_n^m = C _{n-1}^{m-1}+C _{n-1}^{m}
$$

$$
mC_n^m = nC _{n-1}^{m-1}
$$

$$
C_n^0+C_n^1+C_n^2+……+C_n^n = 2^n
$$

$$
1C_n^1+2C_n^2+3C_n^3+……+nC_n^n =n2^{n-1}
$$

$$
1^2C_n^1+2^2C_n^2+3^2C_n^3+……+n^2C _n^n =n(n+1)2^{n-2}
$$

$$
\frac{C_n^1}{1}-\frac{C_n^2}{2}+\frac{C_n^3}{3}+……+(-1)^{n-1}\frac{C _n^n}{n} =1 + \frac{1}{2}+ \frac{1}{3}+……+ \frac{1}{n}
$$

$$
(C_n^0)^2+(C_n^1)^2+(C_n^2)^2+\cdots+(C_n^{n})^2 = C_{2n}^n
$$

## 斐波那契数列

### 通项公式

$$
f_n=\frac{1}{\sqrt{5}}((\frac{1+\sqrt{5}}{2})^n-(\frac{1-\sqrt{5}}{2})^n)
$$

### 前n项求和公式

$$
S_n=f_{n+2}-1=\sum_{i=1}^{n}f_i
$$

### 不知道怎么来的公式

$$
f_{2n}=f_{n+1}\times f_{n}+f_n\times f_{n-1}=f_{n+1}\times f_{n}+f_n\times(f_{n+1}-f_{n})
$$


$$
f_{2n+1}=f_{n+1}\times f_{n+1}+f_n\times f_n
$$

可能会比矩阵快速幂要快

