---
title: 2019-Nowcoder-Multi-University-Training-Camp-3
copyright: right
mathjax: true
top: false
categories: 题解报告
description: 2019牛客多校第三场题解
tags:
  - algorithm
  - 2019-Multi-University-Training-Contest
id: 2019nowcoder3
date: 2019-07-29 21:18:37
---

## solution

### B Crazy Binary String

#### 题意

给你一个串，找出01个数相同的子串和子序列最长的长度为多少。

#### 题解

对于子串，把0换成-1，如果$s[l,r]$的01个数相同，那么前缀和$a[r]-a[l-1]=0$

对于子序列，$\min(count('0'),count('1'))$就是答案。

#### 代码

```cpp
#include <cstdio>
#include <algorithm>
#include <iostream>
using namespace std;
typedef long long ll;
char s[100010];
int a[100010];
ll cal(int n,int m) {
    ll ans=0;
    int minn=min(n,m);
    ll l=1,r=1;
    for(int i=1; i<=minn; i++) {
        l=l*(n--)/i;
        r=r*(m--)/i;
        ans+=l*r;
    }
    return ans;
}

struct node {
    int x,id;
    bool operator <(const node &tmp) const {
        if(x!=tmp.x)return x<tmp.x;
        return id<tmp.id;
    }
} no[100010];
int main() {
    int n;
    scanf("%d",&n);
    scanf("%s",s+1);
    int x=0,y=0;
    no[0].x=0;
    no[0].id=0;
    for(int i=1; i<=n; i++) {
        a[i]=(s[i]=='0')?-1:1;
        a[i]+=a[i-1];
        no[i].x=a[i];
        no[i].id=i;
        if(s[i]=='0')x++;
        else y++;
    }
    int ans=0;
    sort(no,no+1+n);
    int l=0,r=0;
    while(r<=n) {
        if(no[r+1].x==no[l].x) {
            r++;
        } else {
            r=l=r+1;
        }
        ans=max(ans,no[r].id-no[l].id);
    }
    printf("%d %d\n",ans,min(x,y)<<1);
    return 0;
}
```

### F Planting Trees

#### 题意

给你n*n的矩阵，找出最大的矩阵，使得找出的矩阵最大值减最小值<=m。

#### 题解

枚举上下边界，枚举右边界，用两个数组保存上下边界内每一列的最大值和最小值，用两个单调队列维护从L列的R列的最大值和最小值，这样可以找出枚举的右边界对应的左边界。注意，有可能出现找不到左边界，所以此时应该枚举下一个右边界。

#### 代码

```cpp
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <string>
#include <vector>
#include <string>
#include <cmath>
#include <ctime>
#include <queue>
#include<stack>
#include <map>
#include <set>
#include<fstream>e
#define lowbit(x) x&(-x)
using namespace std;
typedef long long ll;
const int N =505;
const int INF = 0x3f3f3f3f;
const int mod = 1e9 + 7;
const double esp = 1e-7;
int n, m, Map[N][N],Max[N],Min[N],q[N],p[N];
int main() {
    int t,ans;
    scanf("%d",&t);
    while (t--) {
        ans = 0;
        scanf("%d%d",&n,&m);
        for (int i = 1; i<= n; i++) {
            for (int j = 1; j <= n; j++) {
                scanf("%d",&Map[i][j]);
            }
        }
        for (int i = 1; i <= n; i++) {
            for (int p = 1; p <= n; p++) {
                Max[p] = Map[i][p], Min[p] = Map[i][p];
            }
            for (int j = i; j <= n; j++) {
                for (int k = 1; k <= n; k++) {
                    Max[k] = max(Max[k],Map[j][k]);
                    Min[k] = min(Min[k], Map[j][k]);
                }
                int l = 1, r = 1, ql = 1, qr = 1, pl = 1,pr = 1;
                q[1] = 1, p[1] = 1;
                while (r <= n) {
                    if (l==r+1||Max[q[ql]] - Min[p[pl]] <= m) {
                        ans = max(ans,(j-i+1)*(r-l+1));
                        r++;
                        while (ql <= qr && Max[q[qr]] <= Max[r])
                            qr--;
                        q[++qr] = r;
                        while (pl <= pr && Min[p[pr]] >= Min[r])
                            pr--;
                        p[++pr] = r;
                    }
                    else {
                        if (q[ql] == l)
                            ql++;
                        if (p[pl] == l)
                            pl++;
                        l++;
                    }
                }
            }
        }
        printf("%d\n",ans);
    }
    return 0;
}
```

### H Magic Line

#### 题意

给你n个点，n为偶数，你要作一条线使得n个点恰好平分在两边，给出这条线过的两个点。

#### 题解

对点双关键字排序，然后把前$\frac{n}{2}$个点划分到一边，其余的划分到另一边，只要直接足够直，就不会穿过给出的点。

#### 代码

``` cpp
#include <algorithm>
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <string>
#include <cmath>
#include <ctime>
#include <queue>
#include <map>
#include <set>
/*
#include <unordered_map>
#include <unordered_set>
*/
using namespace std;
int x10,x20,y10,y20;
typedef long long ll;
struct node {
    int x,y;
    bool operator <(const node &tmp)const {
        if(x!=tmp.x)return x<tmp.x;
        return y<tmp.y;
    }    len=0;//使用的是手写堆，所    len=0;//使用的是手写堆，所以这里我要每次清零。可以忽略以这里我要每次清零。可以忽略

} no[1010];
int x[1010],y[1010];
int main() {
    //freopen("in.txt","r",stdin);
    //freopen("out.txt","w",stdout);
    int t;
    scanf("%d",&t);
    while(t--) {
        int n;
        scanf("%d",&n);
        for(int i=1; i<=n; i++)    len=0;//使用的是手写堆，所以这里我要每次清零。可以忽略 {
            scanf("%d%d",&x[i],&y[i]);
            no[i].x=x[i];
            no[i].y=y[i];
        }
        sort(no+1,no+n+1);
        int mid = n>>1;
        if(no[mid].x!=no[mid+1].x) {
            printf("%d 100000000 %d -100000000\n",no[mid].x,no[mid+1].x);
        } else {
            int tmp = (no[mid+1].y+no[mid].y)>>1;
            int l=tmp+100000000,r=tmp-100000000;
            if(abs(no[mid].y-no[mid+1].y)==1)r++;
            printf("%d %d %d %d\n",no[mid].x-1,l,no[mid].x+1,r);
        }
    }
    return 0;
}
```

### J

#### 代码

```cpp
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <string>
#include <vector>
#include <string>
#include <cmath>
#include <ctime>
#include <queue>
#include<stack>
#include<list>
#include <unordered_map>
#include <set>
#include<fstream>
#define lowbit(x) x&(-x)
using namespace std;
typedef long long ll;
const int N =505;
const int INF = 0x3f3f3f3f;
const int mod = 1e9 + 7;
const double esp = 1e-7;
int main() {
    std::ios::sync_with_stdio(false);
    cin.tie(0);
    int t,n,m,op,v;
    string s;
    cin >> t;
    while (t--) {
        unordered_map<string,list<pair<string,int>>::iterator>M;
        M.clear();
        list<pair<string, int>>L;
        L.clear();
        cin >> n >> m;
        for (int i = 0; i < n; i++) {
            cin >> op >> s >> v;
            pair<string, int> p = make_pair(s, v);
            if (op == 0) {
                if (M.find(p.first) == M.end()) {
                    L.push_back(p);
                    M[p.first] = --L.end();
                    if (L.size() > m) {
                        M.erase(L.front().first);
                        L.pop_front();
                    }
                    printf("%d\n", v);
                }
                else {
                    p = *M[p.first];
                    L.erase(M[p.first]);
                    L.push_back(p);
                    M[p.first] =--L.end();
                    printf("%d\n",p.second);
                }
            }
            else {2019年7月29日 (一) 11:00的版本
                if (M.find(p.first) == M.end()) {
                    printf("Invalid\n");
                }
                else {
                    list<pair<string, int>>::iterator id=M[p.first];
                    if ((id == L.begin() && v == -1) || (id == --L.end() && v == 1)) {
                        printf("Invalid\n");
                        continue;
                    }
                    if (v == -1)
                        id--;
                    else if (v == 1)
                        id++;
                    p = *id;
                    printf("%d\n",p.second);
                }
            }
        }
    }
    return 0;
}
```

