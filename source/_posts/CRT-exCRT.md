---
title: CRT/exCRT
copyright: right
mathjax: true
top: false
categories: 学习笔记
description: 中国剩余定理与扩展中国剩余定理笔记
tags: math
id: crt
date: 2019-04-15 02:15:36
---
## CRT(中国剩余定理)

给你一个方程组
$$
x=a_1 \pmod {m_1}
$$

$$
x=a_2 \pmod{m_2}
$$


$$
x=a_3 \pmod{m_3}
$$

保证每个$m_i$两两互质，求$x$。

我们先臆想一个神奇的解出来，

设$m=\prod_{1}^{n}{m_i}$，$M_i=\frac{m}{m_i}$，$t_i\times M_i=1\pmod {m_i}$。

得 $x=\sum_{i=1}^{n}a_i\times M_i\times t_i$

得出的$x$是在M范围内的唯一解。

### 假装证明

当求$x\bmod m_i$的时候，其他的$a_j\times M_j\times t_j(j\neq i)$都会有$m_i$的这个因子，所以只需考虑$a_i\times M_i\times t_i$这个式子。因为$M_i\times t_i= 1\pmod {m_i}$，所以 $ x=a_i\pmod {m_i} $ 。证毕。

当然这只是其中一个解啦。

## exCRT

不保证每个$m_i$两两互质，求$x$。

考虑两个方程的时候
$$
\begin{cases}
x=a_i \pmod{m_i}\\\\
x=a_j \pmod{m_j}
\end{cases}
$$
我们可以将它改成
$$
\begin{cases}
x=a_i + m_i\times b_i \\\\
x=a_j + m_j\times b_j
\end{cases}
$$

可得
$$
a_i + m_i\times b_i=a_j + m_j\times b_j \Rightarrow m_i\times b_i-m_j\times b_j=a_j-a_i
$$
当$(a_j-a_i)\bmod \gcd(m_i,m_j) \neq 0$时，方程无解，

否则可以通过扩展欧几里得算法得出最小正解$b_i,b_j$，即可得到一条新的式子
$$
x=a_i+m_i\times b_i \pmod{lcm(m_i,m_j)}
$$
当方程数量大于二的时候，可以通过两两合并，