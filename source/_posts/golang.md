---
title: golang
copyright: right
mathjax: true
top: false
categories: 学习笔记
description: go语言环境配置
tags: golang
id: golang1
abbrlink: 2937857443
date: 2019-04-01 14:06:55
---

## 安装go环境

[go的官方网站](https://golang.org)

好像有墙(逃

不愧是出自404公司之手的与语言啊

我大arch系，直接pacman装就好了(逃

可以来[这里](https://golang.google.cn/dl/)下，一直不知道google.cn是谁在维护的，但是能用。

## linux设置

在/etc/profile 中增加一下设置

```
export GOROOT=/usr/lib/go //这是你的go所在的目录
export GOPATH=~/golib:~/goproject
export GOBIN=~/gobin
export PATH=$PATH:$GOROOT/bin:$GOBIN
```

其实很多还是看不懂的（留坑）

修改完之后，输入以下，使配置生效

``` bash
source /etc/profile
```

