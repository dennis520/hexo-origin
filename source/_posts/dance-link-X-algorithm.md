---
title: dance link X algorithm
tags: algorithm
copyright: right
categories: 学习笔记
description: 舞蹈链算法笔记
id: dlx
date: 2018-08-28 17:55:06
top:
---

[图文并茂的dance link X 算法讲解(点击我啊)](http://www.cnblogs.com/grenet/p/3145800.html)

dance link X算法是用来解决精确覆盖问题,例如在一个01矩阵中,找n行,能使得每一列都只有一个1,其余全是0.

而怎么将求解数独转化为DLX能解决的问题呢?

假设是一个9*9的数独,每一行,每一列,每个宫都要有1~9这9个数,每一个格子都有9个状态,所以一共有9*9*9的状态,所以DLX要建729行,然后用9*9来表示每个格子都有数字了,每行每列每宫都要9*9来表示当前行/列/宫有数字k了.一共4*9*9列.
直接上题目吧[POJ3074](http://poj.org/problem?id=3074)
``` cpp
    #include <cstdio>
    #include <cstring>
    using namespace std;
    const int N = 9; //3*3 数独
    const int MaxN = N * N * N + 10;
    const int MaxM = N * N * 4 + 10;
    const int maxnode = MaxN * 4 + MaxM + 10;
    char g[MaxN];
    struct DLX {
        int n, m, size;
        int U[maxnode], D[maxnode], R[maxnode], L[maxnode], Row[maxnode], Col[maxnode];
        int H[MaxN], S[MaxM];//S维护每一列有多少个点,H维护每一行的头
        int ansd, ans[MaxN];
        void init(int _n, int _m) {//m维护每一列,n维护每一行
            n = _n;
            m = _m;
            for (int i = 0; i &lt;= m; i++) {
                S[i] = 0;
                U[i] = D[i] = i;
                L[i] = i - 1;
                R[i] = i + 1;
            }
            R[m] = 0; L[0] = m;
            size = m;
            for (int i = 1; i &lt;= n; i++)H[i] = - 1;
        }
        void Link(int r, int c) {
            ++S[Col[++size] = c];//size像是每个点的编号
            Row[size] = r;
            D[size] = D[c];
            U[D[c]] = size;
            U[size] = c;
            D[c] = size;
            if (H[r] &lt; 0)H[r] = L[size] = R[size] = size;
            else {
                R[size] = R[H[r]];
                L[R[H[r]]] = size;
                L[size] = H[r];
                R[H[r]] = size;
            }
        }
        void remove(int c) {
            L[R[c]] = L[c]; R[L[c]] = R[c];
            for (int i = D[c]; i != c; i = D[i])
                for (int j = R[i]; j != i; j = R[j]) {
                    U[D[j]] = U[j];//断开j
                    D[U[j]] = D[j];
                    -- S[Col[j]];
                }
        }
        void resume(int c) {
            for (int i = U[c]; i != c; i = U[i])
                for (int j = L[i]; j != i; j = L[j])
                    ++S[Col[U[D[j]] = D[U[j]] = j]];
            L[R[c]] = R[L[c]] = c;
        }
        bool Dance(int d) {
            if (R[0] == 0) {
                for (int i = 0; i &lt; d; i++)g[(ans[i] - 1) / 9] = (ans[i] - 1) % 9 + '1';
                for (int i = 0; i &lt; N * N; i++)printf("%c", g[i]);
                printf("\n");
                return true;
            }
            int c = R[0];
            for (int i = R[0]; i != 0; i = R[i])
                if (S[i] &lt; S[c])
                    c = i;
            remove(c);
            for (int i = D[c]; i != c; i = D[i]) {
                ans[d] = Row[i];
                for (int j = R[i]; j != i; j = R[j])remove(Col[j]);
                if (Dance(d + 1))return true;
                for (int j = L[i]; j != i; j = L[j])resume(Col[j]);
            }
            resume(c);
            return false;
        }
    };
    void place(int &amp;r, int &amp;c1, int &amp;c2, int &amp;c3, int &amp;c4, int i, int j, int k) {
        r = (i * N + j) * N + k;
        c1 = i * N + j + 1;
        c2 = N * N + i * N + k;
        c3 = N * N * 2 + j * N + k;
        c4 = N * N * 3 + ((i / 3) * 3 + (j / 3)) * N + k;
    }
    DLX dlx;
    int main() {
        while (scanf("%s", g) == 1) {
            if (strcmp(g, "end") == 0)break;
            dlx.init(N * N * N, N * N * 4);
            int r, c1, c2, c3, c4;
            for (int i = 0; i &lt; N; i++)
                for (int j = 0; j &lt; N; j++)
                    for (int k = 1; k &lt;= N; k++)
                        if (g[i * N + j] == '.' || g[i * N + j] == '0' + k) {
                            place(r, c1, c2, c3, c4, i, j, k);
                            dlx.Link(r, c1);
                            dlx.Link(r, c2);
                            dlx.Link(r, c3);
                            dlx.Link(r, c4);
                        }
            dlx.Dance(0);
        }
        return 0;
    }
```