---
title: Mobius inversion formula
tags:
  - algorithm
  - math
copyright: right
categories: 学习笔记
description: 莫比乌斯反演学习笔记
id: Mobius-inversion-formula
mathjax: true
abbrlink: 734077519
date: 2019-01-24 01:21:51
top:
---

## 整除分块

$$
\sum_{i=1}^{n} \lfloor \frac {n}{i}\rfloor
$$

|               i               |  1   |  2   |  3   |  4   |  5   |  6   |  7   |  8   |  9   |  10  |  11  |  12  |  13  |  14  |  15  |  16  |  17  |  18  |  19  |  20  |
| :---------------------------: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: |
| $\lfloor \frac {n}{i}\rfloor$ |  20  |  10  |  6   |  5   |  4   |  3   |  2   |  2   |  2   |  2   |  1   |  1   |  1   |  1   |  1   |  1   |  1   |  1   |  1   |  1   |

$n/(n/i)$

$n/i$就是要求的答案，$n(n/i)$就是满足大于等于答案的最大值$i_{max}$，每次根据这个范围去求就好了。$n/(n/i)=i_{max}$，就可以看成$k*(n/i)\le n$这个式子就k的最大值。

$O(2\sqrt{n})$

``` cpp
 for(int l=1,r;l<=n;l=r+1){
 	r=n/(n/l);
    ans+=(r-l+1)*(n/l);
 }
```





## 莫比乌斯反演

### 积性函数

$\gcd(x,y)=1$ $f(xy)=f(x)*f(y)$  欧拉函数 莫比乌斯函数

### 完全积性函数

$f(x\cdot y)=f(x)\cdot f(y)$  

Example:  $f(x)=x$

### 狄利克雷卷积

两个数论函数
$$
(f*g)(n)=\sum_{d|n}f(d)g(\frac{n}{d})
$$

#### 单位元

$$
\varepsilon(n)=\mu \cdot 1=\sum_{d|n}\mu(d)=[n=1]
$$

$$
\varepsilon(n)= \begin{cases} 1&n=1\\ 0&else \end{cases}
$$


$$
(\varepsilon*g)(n)=\sum_{d|n}\varepsilon(d)g(\frac{n}{d})=\varepsilon(1)g(\frac{n}{1})=g(n)
$$

恒等函数 1 ：   f(x)=1

### 莫比乌斯函数

$$
\mu(n)= \begin{cases} 1&n=1\\ 0&n\text{ 含有平方因子}\\ (-1)^k&k\text{ 为 }n\text{ 的本质不同质因子个数}\\ \end{cases}
$$

$$
n=p_1^{k_1}p_2^{k_2}....p_t^{k_t}
$$



$$
\sum_{d|n}\mu(d)=[n=1]
$$
 假设一个n有k个质因子
$$
\sum_{d|n}\mu(d)=\sum_{i=0}^{k}C^i_k\times (-1)^i=\sum_{i=0}^{k}C^i_k\times (-1)^i\times 1^{k-i}=(1-1)^k=[k=0]
$$

### 演

设两个函数f(x) g(x)
$$
f=f(n)=\sum_{d|n}g(d)=g\times 1
$$

$$
f*\mu=g\times 1\times \mu=g
$$

$$
g=f*\mu
$$

#### 两个演

$$
f(n)=\sum_{d|n}g(d)\to g(n)=\sum_{d|n}\mu(d)\times f(\frac{n}{d})
$$

$$
f(n)=\sum_{n|d}g(d)\to g(n)=\sum_{n|d}\mu(\frac{d}{n})\times f(d)
$$

下推第二个演

设$k=\frac{d}{n}$
$$
\sum_{n|d}\mu(\frac{d}{n})\cdot f(d)=\sum_{k}\mu(k)f(nk)=\sum_{k}\mu(k)\sum_{(nk)|t}g(t)=\sum_{k}\sum_{(nk)|t}\mu(k)g(t)=\sum_{t}g(t)\sum_{(nk)|t}\mu(k)
$$

$$
\sum_{n|d}\mu(\frac{d}{n})\cdot f(d)=\sum_{t}g(t)\sum_{(nk)|t}\mu(k)=\sum_{t}g(t)\varepsilon(\frac{t}{n})=g(n)
$$

```cpp
void primejudge(int lim) {
    memset(v, 0, sizeof v);
    pr = 0;
    mu[1] = 1;
    for (int i = 2; i <= lim; ++i) {
        if (!v[i]) {
            prime[pr++] = i;
            v[i] = i;
            mu[i] = -1;
        }
        for (int j = 0; j < pr && lim / i >= prime[j]; j++) {
            v[i * prime[j]] = prime[j];
            if (v[i] <= prime[j])break;
            mu[i * prime[j]] = -mu[i];
        }
    }
}
```



## 解题

求这个值$1\le x \le$ a    $1 \le y \le b $求$gcd(x,y)=k$的种数
$$
\sum_{i=1}^a\sum_{j=1}^b=[gcd(i,j)=k]
$$

$$
\sum_{i=1}^{\lfloor \frac{a}{k} \rfloor}\sum_{j=1}^{\lfloor \frac{b}{k} \rfloor}=[gcd(i,j)=1]
$$

$$
f(n)=\sum_{n|d}g(d)\to g(n)=\sum_{n|d}\mu(\frac d n)\cdot f({d})
$$

​                                            $\to$ 函数解释 ：  f(x)为x|gcd(i,j)的个数           g(x)    gcd(i,j)=x的个数
$$
f(m)=\lfloor \frac{a}{m}\rfloor \times \lfloor \frac{b}{m}\rfloor
$$

$$
g(1)=\sum_{1|d}\mu(\frac d 1)\cdot f({d})=\sum_{1|d}\mu(d)\cdot f({d})
$$

