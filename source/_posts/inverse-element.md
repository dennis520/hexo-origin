---
title: Inverse-Element
copyright: right
mathjax: true
tags: algorithm
categories: 学习笔记
description: 逆元学习笔记
id: inverse-element
top: false
abbrlink: 1524290307
date: 2018-11-10 18:15:58
---

### 逆元定义 ###

$a=\frac{1}{b}(mod\ p)$，也就是$a*b=1(mod\ p)$。表示$a$为$b$在$mod\ p$意义下的逆元

#### 快速幂求逆元 ####

$$a^p=a(mod\ p)$$

用二项式定理证明

$$a^p=((a-1)+1)^p=\sum_{i=0}^p{C_p^i*(a-1)^i}$$

来康康这里面的组合数鸭

$$C_n^m=\frac{n!}{m!*(n-m)!}$$

可以发现当n是素数的时候，在$1\le m\le n-1$的项里面都有$n$这个因子，所以所以模运算剩下的只有

$$C_p^0*(a-1)^0$$

$$C_p^p*(a-1)^p$$

也就是

$$a^p=(a-1)^p+1(mod\ p)$$

用数学归纳法就可得

$$a^p=a(mod\ p)$$

$$a^{p-1}=1(mod\ p)$$

$$a^{p-2}=\frac{1}{a}(mod\ p)$$

这样就得到$a$在$mod\ p$意义下的逆元了。