---
title: 牛顿迭代法
copyright: right
mathjax: true
top: false
layout: true
date: 2019-08-16 02:42:53
categories: 学习笔记
description: 牛顿迭代法，一个快速求根的方法
tags: math
id: Newton's-method
---

### 为什么会牛顿迭代法

伽罗瓦用群论证明了高于四次的方程不存在求根公式，那么需要一种快速求方程根的做法了。

牛顿迭代法是求方程根的重要方法之一，其最大优点是在方程的单根附近具有[平方收敛][^1]，比二分求根快了很door。

### 牛顿迭代的原理

~~随便~~选取一个点，做函数的切线，你可以计算出这条切线与x轴上的交点的x坐标，显然是要靠近根了，做多几遍就会越靠近了。

### 牛顿迭代的公式

上面的描述，先选取一个$x_0$，做切线找与x轴上的交点，$x_1=x_0-\frac{f(x_0)}{f'(x_0)}$，也就是按照$x_i=x_{i-1}-\frac{f(x_{i-1})}{f'(x_{i-1})}$一直迭代，可以按照精度迭代，也可以自定数量迭代。

![NewtonIteration.gif](https://cdn.jsdelivr.net/gh/denniszy/cdn/images/NewtonIteration.gif)

### 牛顿迭代适用范围

- 只能用来求方程的一个根，可导即可。
- 多根函数可能会出大锅，而且对初始值有要求

### 实例

#### 开平方

```cpp
double sqrt(double x)
{
    double ans=x*0.5;
   	while (abs(ans*ans-x)>1e-7) {
    	ans-=(ans*ans-x)/(2*ans);
	}
    return x0;
}
```

#### exp

对于$e^b=x$，我们需要求x时，你可以设函数为$y=x-e^b$，但你先不正要求这个$e^b$吗？所以你需要把函数设成$y=ln(x)-b$来做牛顿迭代。

设函数为$y=\ln(x)-b$，求得根时，我们就知道$e^x$的值。

[^1]:[当我们谈论收敛速度时，我们都在谈什么？](https://zhuanlan.zhihu.com/p/27644403)

