---
title: 2019-Multi-University-Training-Contest-1
copyright: right
mathjax: true
top: false
categories: 题解报告
description: 2019杭电多校第一场题解
tags:
  - algorithm
  - 2019-Multi-University-Training-Contest
id: 2019nowcoder1
date: 2019-07-28 17:44:16
---

## solution

### E.Path

题意：给你n个点和m条边，让你断开若干条边，使得从1到n的最短路长度变大或者不连通，求最小花费的花费为多少。断开一条边花费为对应边的长度(边权)。

题解：跑最短路算法，求出能够构成最短路的边，根据这些边建一个新图，跑最小鸽。

我写这个的时候人都傻了，找最短路的边的bfs写歪了，然后就超时了QAQ。

```cpp
#include <algorithm>
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <string>
#include <cmath>
#include <ctime>
#include <queue>
#include <map>
#include <set>
/*
#include <unordered_map>
#include <unordered_set>
*/
using namespace std;
const int INF = 0x3f3f3f3f;
bool vis[10010];
int n,m;
namespace dijk {
    typedef long long ll;
    typedef pair<ll,int> PLI;
    const int N = 10010;
    int Head[N],nxt[N],ver[N];
    ll c[N],d[N];
    int tot;
    void init() {
        tot=1;
        fill(Head,Head+n+1,0);
    }
    void add(int u,int v,ll w) {
        ver[tot]=v;
        c[tot]=w;
        nxt[tot]=Head[u];
        Head[u]=tot++;
    }
    void dijk() {
        fill(d+1,d+n+1,0x3f3f3f3f3f3f3f3f);
        fill(vis,vis+n+1,0);
        d[1]=0;
        priority_queue<PLI,vector<PLI>,greater<PLI>>q;
        q.push(make_pair(0,1));
        while(q.size()) {
            PLI tmp = q.top();
            q.pop();
            int u = tmp.second;
            if(vis[u])continue;
            vis[u]=1;
            for(int i=Head[u]; i; i=nxt[i]) {
                int v = ver[i];
                if(d[v]>d[u]+c[i]) {
                    d[v]=tmp.first+c[i];
                    q.push(make_pair(d[v],v));
                }
            }
        }
    }
};
typedef long long ll;
class ISAP {
  private:
    static const int N = 10010;
    static const int M = 20010;
    static const int INF = 0x3f3f3f3f;
    int tot,n,m,s,t;
    int carc[N],gap[N];
    int pre[N];
    int Head[N],nxt[M],ver[M];
    ll flow[M];
    int d[N];
    bool visited[N];
  public:
    void init(int _n,int _m,int _s,int _t) {
        tot=1;
        n=_n,m=_m,s=_s,t=_t;∫∞011+x2dx=π2
        fill(Head,Head+n+1,0);
    }

    void addedge(int u,int v,ll w) {
        ver[++tot]=v;
        flow[tot]=w;
        nxt[tot]=Head[u];
        Head[u]=tot;

        ver[++tot]=u;
        flow[tot]=0;
        nxt[tot]=Head[v];
        Head[v]=tot;
    }
    bool bfs() {
        fill(visited,visited+n+1,0);
        queue<int>q;
        visited[t]=1;
        d[t]=0;
        q.push(t);
        while(q.size()) {
            int u = q.front();
            q.pop();
            for(int i = Head[u]; i; i=nxt[i]) {
                int v = ver[i];
                if(i&1&&!visited[v]) {
                    visited[v]=true;
                    d[v]=d[u]+1;
                    q.push(v);
                }
            }
        }
        return visited[s];
    }
    ll aug() {
        int u=t;
        ll df=INF;
        while(u!=s) {
            df=min(df,flow[pre[u]]);
            u=ver[pre[u]^1];
        }
        u=t;
        while(u!=s) {
            flow[pre[u]]-=df;
            flow[pre[u]^1]+=df;
            u=ver[pre[u]^1];
        }
        return df;
    }
    ll maxflow();
} flow;
ll ISAP :: maxflow() {
    ll ans=0;
    fill(gap,gap+n+1,0);
    copy(Head+1,Head+n+1,carc+1);
    bfs();
    for(int i=1; i<=n; i++)gap[d[i]]++;
    int u = s;
    while(d[s]<=n) {
        if(u==t) {
            ans+=aug();
            u=s;
        }
        bool advanced=false;
        for(int i=carc[u]; i; i=nxt[i]) {
            if(flow[i]&&d[u]==d[ver[i]]+1) {
                advanced=true;
                pre[ver[i]]=i;
                carc[u]=i;//carc
                u=ver[i];
                break;
            }
        }
        if(!advanced) {
            int mindep=n-1;
            for(int i=Head[u]; i; i=nxt[i]) {
                if(flow[i]) {
                    mindep=min(mindep,d[ver[i]]);
                }
            }
            if(--gap[d[u]]==0)break;
            gap[d[u]=mindep+1]++;

            carc[u]=Head[u];
            if(u!=s)u=ver[pre[u]^1];
        }
    }
    return ans;
}
int main() {
    //freopen("in.txt","r",stdin);
    //freopen("out.txt","w",stdout);
    int t;
    scanf("%d",&t);
    while(t--) {
        scanf("%d%d",&n,&m);
        dijk::init();
        for(int i=1,x,y; i<=m; i++) {
            ll z;
            scanf("%d%d%lld",&x,&y,&z);
            dijk::add(x,y,z);
        }
        dijk::dijk();
        if(dijk::d[n]==0x3f3f3f3f3f3f3f3f) {
            printf("0\n");
            continue;
        }
        flow.init(n,10000,1,n);
        {
            fill(vis,vis+n+1,0);
            queue<int>q;
            q.push(1);
            vis[1]=1;
            while(q.size()) {
                int u = q.front();
                q.pop();
                for(int i=dijk::Head[u]; i; i=dijk::nxt[i]) {
                    int v = dijk::ver[i];
                    if(dijk::d[u]+dijk::c[i]==dijk::d[v]) {
                        flow.addedge(u,v,dijk::c[i]);
                        if(!vis[v])q.push(v);
                        vis[v]=1;
                    }
                }## feature test
            }
        }
        ll ans=flow.maxflow();
        printf("%lld\n",ans==0?-1:ans);
    }
    return 0;
}
```





