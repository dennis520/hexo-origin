---
title: NPM Install Error
copyright: right
mathjax: true
top: false
categories: 折腾笔记
description: npm权限不足的解决方法
tags:
  - npm
id: npm-install-error
abbrlink: 1485080459
date: 2019-08-03 02:56:32
---
>  [Resolving EACCES permissions errors when installing packages globally](https://docs.npmjs.com/resolving-eacces-permissions-errors-when-installing-packages-globally)

> Note: This section does not apply to Microsoft Windows.

Just follow the next few steps:

```shell
mkdir ~/.npm-global
npm config set prefix '~/.npm-global'
```

Open your editor to edit ‘~/.profile’. If this file doesn’t exist, just create it. And add this line:

```shell
export PATH=~/.npm-global/bin:$PATH
```

Update your system variables:

```shell
source ~/.profile
```

If you use cnpm, just change npm to cnpm.

