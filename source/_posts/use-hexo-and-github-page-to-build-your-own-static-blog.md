---
title: Using hexo
copyright: right
mathjax: true
top: false
layout: true
date: 2019-08-26 13:55:34
categories: 折腾笔记
description: use hexo and github page to build your own static blog
tags:
	- Hexo
	- github page
id: hexo-blog
---

### 初步认识

hexo是一个博客框架，通过node.js将你的markdown文件转换为一片博文，搭配风格各异的主题来构建你自己的静态博客。

> [hexo官网](https://hexo.io)

#### 入门配置

#### 安装hexo-cli

```
npm install hexo-cli -g
```

#### 初始化hexo目录

```
hexo init <folder>
cd <folder>
npm install
```

这里可以在`npm install`前修改hexo目录下的package.json，添加你所需要的依赖，或者`npm install`之后再执行下面的操作。

初始化后目录结构如下

```
.
├── _config.yml
├── package.json
├── scaffolds
├── source
|   ├── _drafts
|   └── _posts
└── themes
```

scaffolds 文件夹保存着你的模板文件，你可以通过`hexo new [layout] <title>`来利用模板文件来生成你需要的markdown文件，详细请看[官网文档](https://hexo.io/zh-cn/docs/writing) 

例如我用`hexo new "title"`来生成一个即将发表的文件。

#### 安装部署器并部署到github

##### 安装命令

```
npm install hexo-deployer-git --save
```

##### 在`_config.yml`设置

```
deploy:
  type: git
  repo: ##你的github仓库
  branch: master ##你github仓库做githubpage的分支
  message: ##缺省时，commit的message为部署的时间
```

##### 部署

`hexo g`可以生成整个网站，保存在public文件夹中

`hexo d`可以部署网站到远端，当然可以部署到github

`hexo g -d`和`hexo d -g`都能做到生成后并部署到远端。

### 进阶选项(可选)

#### 安装feed插件

##### 安装命令

```
npm install hexo-generator-feed --save
```

##### 在`_config.yml`设置

```
feed:
  type: atom
  path: atom.xml
  limit: 20
  hub:
  content: true
  content_limit: 20
  content_limit_delim: ' '
  order_by: -date
```

[feed详细设置](https://github.com/hexojs/hexo-generator-feed)

#### 获得置顶功能

##### 安装命令

```
npm uninstall hexo-generator-index --save
npm install hexo-generator-index-pin-top --save
```

##### 在`_config.yml`设置

```
index_generator:
  path: ''
  per_page: 10
  order_by: -date
```

[置顶插件详细设置](https://github.com/netcan/hexo-generator-index-pin-top)

如果某篇文章需要置顶，需要在文章的`Front-matter`中添加`top: true`，否则`top: false`

#### 获得搜索功能

##### 安装命令

```
npm install hexo-generator-search --save
```

##### 在`_config.yml`设置

```
search:
  path: search.xml
  field: post
  content: true
```

[搜索功能详细设置](https://github.com/wzpan/hexo-generator-search)

#### 生成站点地图

可以提交给搜索引擎，加快搜索引擎收录

##### 安装命令

```
npm install hexo-generator-sitemap --save
```

##### 在`_config.yml`设置

```
sitemap:
    path: sitemap.xml
    template:
```

[站点地图插件详细设置](https://github.com/hexojs/hexo-generator-sitemap)

#### 计算文章长度和估算阅读时间

##### 安装命令

```
npm install hexo-symbols-count-time --save
```

##### 在`_config.yml`设置

```
symbols_count_time:
  symbols: true
  time: true
  total_symbols: true
  total_time: true
  exclude_codeblock: true
```

[插件详细配置](https://github.com/theme-next/hexo-symbols-count-time)

### 主题NexT配置

[官方repo](https://github.com/theme-next/hexo-theme-next)

#### 加载条安装

```
git clone https://github.com/theme-next/theme-next-pace source/lib/pace
```

#### 炫酷的canvas-nest

```
git clone https://github.com/theme-next/theme-next-canvas-nest source/lib/canvas-nest
```

克隆之后，还要在NexT的配置文件里进行对应的配置，配置文件中已经说明每个功能的依赖和信息，更多的请根据NexT内的`_config.yml`进行配置

### 注意

这不是教程，这个只是记录我自己是怎么配置的，更多炫酷的配置需要去发掘，比如魔改theme内的.swig文件等等。

### 附件

我的`post.md`

```
---
title: {{ title }}
copyright: right
date: {{ date }}
mathjax: true
top: false
categories:
description:
tags:
id: 
layout: true
---

```