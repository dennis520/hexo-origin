---
title: dogcom
copyright: right
mathjax: true
top: false
layout: true
date: 2019-09-04 23:05:38
categories: 折腾笔记
description: 使用C语言版本的心跳包程序
tags:
	- linux
	- drcom
	- dogcom
id: dogcom
---

### 背景

linux版的drcom无法使用，为了使用校园网，我也是莫的办法，~~逼良为娼~~，

在linux上通过pppoe连接校园网，由于学校需要特定的心跳包，所以需要自己给服务器发心跳包来维持pppoe连接。

之前是通过python版本的程序来发送心跳包的，[源程序](https://github.com/drcoms/drcom-generic/blob/master/latest-pppoe.py)来源于[drcoms/drcom-generic](https://github.com/drcoms/drcom-generic)

### 动机

python版本占的内存有点多，~~才几十M，你真的是关心那几十M内存吗？况且换C程序，内存占用也没得到怎么样的优化~~

python版本的程序，在校园网连接掉了的时候，就会疯狂发送，直到连接正常，所以有时候一脚把网线踢掉的时候，电脑会起飞，发烫，疯狂log，有次它给我log到了11G，~~我不会改啊，不是很熟悉python~~

### dogcom使用

1. windows打开drcom，打开wireshark，使用wireshark抓取drcom登录时的包，保存下来
2. 打开[Auto Configure](https://drcoms.github.io/drcom-generic/)，选择自己的drcom版本，点击open，选择你的抓包文件，得到配置文本，保存为`dogcom.conf`
3. 找个地方把仓库给克隆下来，运行`git clone https://github.com/mchome/dogcom.git`
4. 进入项目文件夹，编译，运行命令`make force_encrypt=y`，过亿亿亿亿亿亿亿亿亿会你就拥有了dogcom文件了，这是linux的操作，windows的操作请看项目主页
5. 把`dogcom`文件和`dogcom.conf`文件放到你一个你想运行的文件夹中
6. 运行，比如我是通过pppoe上网的，就运行`dogcom -m pppoe -c dogcom.conf`

更多的参数配置，请到项目官网查看，~~其实我不会~~

### python版本心跳包使用

也差不多，通过抓包文件得到配置文本之后，修改[python源程序](https://github.com/drcoms/drcom-generic/blob/master/latest-pppoe.py)的`server` `pppoe_flag`  `keep_alive2_flag`的指即可，我以前运行的是python2版本的，运行命令`python2 latest-pppoe.py`即可，它会在你的运行目录生成一个log文件

