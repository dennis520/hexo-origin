---
title: Bayes'theorem
copyright: right
mathjax: true
top: false
description: 贝叶斯公式学习笔记
tags:
  - math
  - Probability theory
categories: 学习笔记
id: bayes
date: 2019-03-10 15:31:21
---

### 贝叶斯公式 ###

$$
P(A_i|B)=\frac{P(A_i)P(B|A_i)}{\sum_{j=1}^{n}P(A_j)P(B|A_j)}
$$

一见面先抛公式。

#### 作用 ####

当公式左边的概率难以求得的时候，我们可以通过右面的公式求得左边的值，有点像反演的思想。

#### 推 ~~倒~~ 导 ####

前置技能

##### 条件概率 #####

当发生事件X的时候，发生事件Y的概率。

即是当两件事情同时发生的概率除以发生X发生的概率。
$$
P(Y|X)=\frac{P(XY)}{P(X)}
$$

##### 全概率公式#####

将样本空间划分成$n$件相互独立事件，求事件X的发生的概率，就是样本空间每件独立事件与事件X同时发生的概率乘每件事件发生的概率，然后全部加起来即可。
$$
P(X)=\sum_{i=1}^{n} {P(Y_i)P(X|Y_i)}
$$

##### 开冲！#####

$$
P(A_i|B)=\frac{P(A_{i}B)}{P(B)}=\frac{P(A_i)P(B|A_i)}{\sum_{j=1}^{n}P(A_j)P(B|A_j)}
$$

就是下面用全概率公式即可推到得出贝叶斯公式。