---
title: manjaro下使用tenda-U12
copyright: right
mathjax: true
top: false
categories: 折腾笔记
description: linux网卡使用
tags:
  - manjaro
  - linux
id: manjaro2
abbrlink: 2668016324
date: 2019-07-13 17:07:25
---

### 一句话

在manjaro上搜索[rtl8812au](https://www.realtek.com/en/products/communications-network-ics/item/rtl8812au)的驱动装一下，我装的名字是rtl8812au-dkms-git(在archlinuxcn的仓库下，可以自己添加一下仓库)，然后把网卡插进去就能识别使用了。

### 心路历程

官网有这个网卡的windows/linux/macos的驱动包，然而这个包根本不能编译成功，我的内核也在他要求的范围之内，要求是2.6.18至4.4，我的是……4……….19打扰了，然后自己上网找找教程，上github找找对应的驱动clone下来编译安装？找了一个编译一下，编译失败，忽然想到去aur上看看有没有，用yay搜了一下，发现archlinuxcn库里面有，就装了，就完事了。