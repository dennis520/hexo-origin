---
title: 2019-Nowcoder-Multi-University-Training-Camp-1
copyright: right
mathjax: true
top: false
categories: 题解报告
description: 2019牛客多校第一场题解
tags:
  - algorithm
  - 2019-Multi-University-Training-Contest
id: 2019nowcoder1
date: 2019-07-28 17:44:16
---

## solution

### A Equivalent Prefixes

题意：给你ab两个数组，让你找出[1,p]区间使得他们任一区间的最小值的位置是一样的，求最大的p。

题解：二分答案，分别建立笛卡尔树，树形一样则正确。

### B Integration

题意：知道$\int_{0}^{\infty} \frac{1}{1+x^2}dx=\frac{\pi}{2}$，求$\int_{0}^{\infty} \frac{1}{\prod_{i=1}^n{a_i^2+x^2}}dx$，可以证明这个一定是分数，要我们用分子乘分母逆元的方式来表示它。

题解：将连乘转换为累加，我们知道

$$
\frac{1}{xy}=\frac{1}{y-x}\times\frac{y-x}{xy}=\frac{1}{y-x}\times(\frac{1}{x}-\frac{1}{y})
$$

对于上面的连乘可以转换为

$$
\int_{0}^{\infty} \frac{1}{(a^2+x^2)\times(b^2+x^2)}dx=\int_{0}^{\infty} \frac{1}{b^2-a^2}\times(\frac{1}{a^2+x^2}-\frac{1}{b^2+x^2})dx=\frac{1}{b^2-a^2}\int_{0}^{\infty} \frac{1}{a^2+x^2}-\frac{1}{b^2+x^2}dx
$$

$$
\int_{0}^{\infty} \frac{1}{(b^2+x^2)\times(c^2+x^2)}dx=\frac{1}{c^2-b^2}\int_{0}^{\infty} \frac{1}{b^2+x^2}-\frac{1}{c^2+x^2}dx
$$

$$
\int_{0}^{\infty} \frac{1}{(a^2+x^2)\times(b^2+x^2)\times{(c^2+x^2)}}dx=\int_{0}^{\infty} \frac{1}{c^2-a^2}\times(\frac{1}{(a^2+x^2)\times(b^2+x^2)}-\frac{1}{(b^2+x^2)\times(c^2+x^2)})dx
$$

由此我们可以简略证明出

$$
\int_{0}^{\infty} \frac{1}{\prod_{i=n}^m{a_i^2+x^2}}dx(n \lneq m)
$$

可以由

$$
\int_{0}^{\infty} \frac{1}{\prod_{i=n+1}^m{a_i^2+x^2}}dx$$和$$\int_{0}^{\infty} \frac{1}{\prod_{i=n}^{m-1}{a_i^2+x^2}}dx
$$

得到。

而如果不预处理的话，直接爆搜会导致$$O(2^n)$$的时间复杂度。

```cpp
#include <cstdio>
#include <cmath>
#include <algorithm>
using namespace std;
typedef long long ll;
 
const long long mod = 1e9+7;
ll qpow(ll a,ll n){
    ll ans=1;
    while(n){
        if(n&1)ans=ans*a%mod;
        n>>=1;
        a=a*a%mod;
    }
    return ans;
}
int n;
ll a[1010];
long long get(int x){
    long long ans=1;
    for(int i=1;i<=n;i++){
        if(i==x)continue;
        ans=ans*((a[i]*a[i]-a[x]*a[x])%mod)%mod;
        ans=(ans+mod)%mod;
    }
    return ans;
}
int main(){
    while(~scanf("%d",&n)){
        for(int i=1;i<=n;i++){
            scanf("%lld",a+i);
        }
        long long ans=0;
        for(int i=1;i<=n;i++){
            ans=(ans+qpow(2*a[i],mod-2)*qpow(get(i),mod-2))%mod;
        }
        printf("%lld\n",ans);
    }
    return 0;
}
```



### E ABBA

题意：给你两个数$n$和$m$,找出$2(n+m)$长度的只包含字母A和B的字符串符合有n个AB子序列和m个BA子序列的字符串有多少种方案。每个字母自能用一次。

题解：动态规划，dp[i,j]定义为字符串前缀(i+j)长度时候，有i个字母A和j个字母B的方案数，对于转移方程，对字母B的数量比需要的B的数量多的时候和字母A出现相同情况的状态作为非法状态，不对非法状态进行转移，通过计算当前状态是否能加上字母A或者字母B,如果可以的话，就进行对应的状态转移。

```cpp
#include <algorithm>
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <string>
#include <cmath>
#include <ctime>
#include <queue>
#include <map>
#include <set>
/*
#include <unordered_map>
#include <unordered_set>
*/
using namespace std;
const long long mod = 1e9+7;
int n,m;
inline void add(long long &a,long long x){
    a+=x;
    if(a>=mod)a-=mod;
}
long long dp[2010][2010];
int main(){
    //freopen("in.txt","r",stdin);
    //freopen("out.txt","w",stdout);
    while(scanf("%d%d",&n,&m)==2){
        int s = n+m;
        //memset(dp,0,sizeof dp);
        for(int i=0;i<=s;i++){
            for(int j=0;j<=s;j++){
                dp[i][j]=0;
            }
        }
        dp[0][0]=1;
        for(int i=0;i<=s;i++){
            for(int j=0;j<=s;j++){
                int ca=0,cb=0;
                int xy=max(n-i,0);
                int x=min(i,n)-max(j-m,0);
                if(x<0)continue;
                ca+=xy;
                cb+=x;
                 
                xy=max(m-j,0);
                x=min(j,m)-max(i-n,0);
                if(x<0)continue;
                cb+=xy;
                ca+=x;
         
                if(ca>0){
                    add(dp[i+1][j],dp[i][j]);
                }
                if(cb>0){
                    add(dp[i][j+1],dp[i][j]);
                }
            }
        }
        printf("%lld\n",dp[s][s]);
    }
    return 0;
}s
```



### F Random Point in Triangle

题意：给你三角形的三个点，在三角形中均匀地取点P，求$$E = \max \{S_{PAB},S_{PBC},S_{PCA}\}$$的期望。

题解：手动作了个正三角形，手动算出三角形面积和期望x36的关系，然后就过了。

```cpp
#include <cstdio>
#include <cmath>
#include <algorithm>
using namespace std;
struct vec{
    long long x,y;
    vec(long long _x=0,long long _y=0):x(_x),y(_y){};
    long long corss(const vec &tt){
        return x*tt.y-y*tt.x;
    }
};
int main(){
    long long x1,x2,x3,y1,y2,y3;
    while(~scanf("%lld%lld%lld%lld%lld%lld",&x1,&y1,&x2,&y2,&x3,&y3)){
        vec a(x2-x1,y2-y1),b(x3-x1,y3-y1);
        long long ans= abs(a.corss(b)*11);
        printf("%lld\n",ans);
    }
    return 0;
}
```



### H XOR

题意：给你$$n$$个数字，作为一个集合的元素，找出子集异或为0的大小的和。

题解：线性基

对于集合大小求和会有点难以求得，我们可以将问题转换为计算每个数的贡献，先找出整个集合构成线性基的$k$个数，那么在$n-k$个数构成的集合中任一非空集合都能在线性基找到若干个数使得异或为$0$，所以在$n-k$个数，每个数都能参与$2^{n-k-1}$个集合的构造，所以$n-k$个数的贡献都为$2^{n-k-1}$，那么剩余那k个数需要计算贡献，用$n-k$个数建立另外一个线性基，每次先把$k$个数中的$k-1$个插入第二个线性基，然后检查第二个线性基是否需要这唯一的数来构造生成整个数组的线性基，如果不需要，这唯一的数也能参与到异或为$0$的子集中的构造，那么由于线性基的性质，相同集合的线性基都能由同样数量的k个数来构造，那么这唯一的数的贡献也就是$^{n-k-1}$了，然后把k个数都试一遍就行了。

```cpp
#include <algorithm>
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <string>
#include <cmath>
#include <ctime>
#include <queue>
#include <map>
#include <set>
/*
#include <unordered_map>
#include <unordered_set>
*/
using namespace std;
const long long mod = 1e9+7;
const int MAXL=62;
struct basis {
    long long a[MAXL+1];
    basis() {
        reset();
    }
    void reset() {
        memset(a,0,sizeof a);
    }
    bool insert(long long x) {
        for(int i=MAXL; i>=0; --i) {
            if(!(x>>i)&1)continue;
 
            if(a[i])x^=a[i];
            else {
                for(int j=0; j<i; j++)if((x>>j)&1)x^=a[j];
                for(int j=i+1; j<=MAXL; j++)if((a[j]>>i)&1)a[j]^=x;
                a[i]=x;
                return true;
            }
        }
        return false;
    }
    long long qmax() {
        long long ans=0;
        for(int i=0; i<=MAXL; i++)ans^=a[i];
        return ans;
    }
} a,b,c;
int n;
long long ans,w,tmp;
long long qpow(long long a,long long n) {
    long long ans=1;
    while(n) {
        if(n&1)ans=ans*a%mod;
        n>>=1;
        a=a*a%mod;
    }
    return ans;
}
long long aa[MAXL+10],top;
int main() {
    //freopen("in.txt","r",stdin);
    //freopen("out.txt","w",stdout);
    while(~scanf("%d",&n)) {
        a.reset();
        b.reset();
        top=ans=w=0;
        for(int i=1; i<=n; i++) {
            scanf("%lld",&tmp);
            if(a.insert(tmp)) {
                w++;
                aa[++top]=tmp;
            } else {
                b.insert(tmp);
            }
        }
        ans=(n-w);
        if(ans==0) {
            puts("0");
            continue;
        }
        for(int i=1; i<=top; i++) {
            c=b;
            for(int j=1; j<=top; j++) {
                if(i!=j)c.insert(aa[j]);
            }
            if(!c.insert(aa[i])) {
                ans++;
            }
        }
        printf("%lld\n",ans*qpow(2ll,n-1-w)%mod);
    }
    return 0;
}
```



### J Fraction Comparision

题意：
比较两个分数的大小，其中分子范围1e18，分母1e9

题解:
将两分数分别分为整除数与模数，再比较大小即可。

```cpp
#include <algorithm>
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <string>
#include <cmath>
#include <ctime>
#include <queue>
#include <map>
#include <set>
/*
#include <unordered_map>
#include <unordered_set>
*/
using namespace std;
const double EPS=1e-9;
int panduan(long double x,long double y) {
    if(fabs(x-y)<EPS)return 0;
    if(x>y)return 1;
    else return -1;
}
int main() {
    //freopen("in.txt","r",stdin);
    //freopen("out.txt","w",stdout);
    long long x,y,a,b;
    while(~scanf("%lld%lld%lld%lld",&x,&a,&y,&b)) {
        if(x/a>y/b) {
            puts(">");
        } else if(x/a<y/b) {
            puts("<");
        } else {
            x%=a;
            y%=b;
            if(x*b>y*a)puts(">");
            else if(x*b<y*a)puts("<");
            else puts("=");
        }
    }
    return 0;
}
```

