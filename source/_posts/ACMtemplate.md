---
title: Template
copyright: right
mathjax: true
top: false
categories: 资源
description: ACM常用算法模板
tags:
  - algorithm
  - template
id: acmtemplate
date: 2019-07-29 02:24:02
---

### Introducing

We are working on a new ACMtemplate for preparing the coming contest. But it is a long job. We need to take some time to collect template and refactor it in order to make it useful. It has two versions. One is made from markdown by using typora, another one is made from $\LaTeX$ by using $\TeX{studio}$.

This is the [github repo](https://github.com/DennisZY/ACMtemplate). I would be happy to have your template contributed.

### Download

#### source code

- [markdown](https://raw.githubusercontent.com/DennisZY/ACMtemplate/master/ACM-template-markdown-version.md)
- [latex](https://raw.githubusercontent.com/DennisZY/ACMtemplate/master/ACM-template-latex-version.tex)
