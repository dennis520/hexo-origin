---
title: Privacy
copyright: right
mathjax: true
top: false
layout: true
date: 2019-08-07 21:32:49
categories: 折腾笔记
description: 你总会不知不觉地用你的隐私交换成方便
tags:
	- privacy
id: privacy
---

### DNS

DNS加密，使用DoH加密域名解析请求。

使用Cloudflare的公共DNS，不让DNS记录你的域名解析内容。

### Search Engine

使用[DuckDuckGo](https://duckduckgo.com/)或者[秘迹搜索](https://mijisou.com/)，进行搜索，不记录你的搜索记录。

### Password

少依赖电子程序或者电子文档去记录密码，可以利用电子程序去生成随机密码，尽量用纸笔记录帐号密码，这有利于你知道你在哪些网站注册过，销号容易，换手机号码也方便。