---
title: 入坑QT_1-最简单的项目理解
copyright: right
mathjax: true
top: false
layout: true
date: 2019-09-12 01:58:32
categories: Qt开发
description: 我好类啊
tags:
- Qt
- c++
id: qt1
---

先上类图

![img](https://cdn.jsdelivr.net/gh/denniszy/cdn/images/qwidget1.svg)

项目分为Forms、Sources、Headers，这只是再Qt Creator里面被分为这个样子，项目文件夹里面是没有分开的。

Qt先将ui文件生成为ui_xxxx.h，内含一个类Ui_xxx，提供setui函数，通过接收窗体指针来对UI进行修改，~~有点抽象工厂的意思~~，然后通过这个类派生出一个在命名空间Ui里的类，相当于一个配置类，谁需要这个配置就用这个类的setui来设置自己。

