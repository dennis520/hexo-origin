---
title: Beatty's theory
copyright: right
mathjax: true
top: false
categories: 学习笔记
description: 贝蒂定理学习笔记
tags:
  - math
id: Beatty
date: 2019-07-15 22:44:37
---

### 定理

当正无理数$x$、$y$满足下列式子时
$$
\frac{1}{x}+\frac{1}{y}=1
$$
会有$P=\{\lfloor{nx}\rfloor|n \in N^+\}$,$Q=\{\lfloor{ny}\rfloor|n \in N^+\}$,使得集合P和集合Q正好是$Z^+$的一个划分,即$P\cup Q=Z^+$、$P\cap Q=\emptyset$.

### 证明

#### 1.一个正整数在集合P或集合Q中至多出现一次

因为$x>1$、$y>1$，所以$\lfloor{nx}\rfloor$不会存在相同的正整数，$\lfloor{ny}\rfloor$亦然。

#### 2.$P\cap Q=\emptyset$

反证法来一手，假设有正整数n、m、k使得$\lfloor{nx}\rfloor=\lfloor{my}\rfloor=k$，即
$$
k<nx<k+1
$$

$$
k<my<k+1
$$

转化一下
$$
\frac{n}{k+1}<\frac{1}{x}<\frac{n}{k}
$$

$$
\frac{m}{k+1}<\frac{1}{y}<\frac{m}{k}
$$

两式相加
$$
\frac{n+m}{k+1}<\frac{1}{x}+\frac{1}{y}=1<\frac{n+m}{k}
$$
再转化一下
$$
k<n+m<k+1
$$
因为n、m是正整数，所以不符合，所以$P\cap Q=\emptyset$

#### 3.$P\cup Q=Z^+$

继续反证法，假设有正整数n、m、k使得$\lfloor{nx}\rfloor<k<\lfloor{(n+1)x}\rfloor$、$\lfloor{my}\rfloor<k<\lfloor{(m+1)y}\rfloor$

再进一步说
$$
\lfloor{nx}\rfloor<nx<k\le\lfloor{(n+1)x}\rfloor-1<(n+1)x-1<\lfloor{(n+1)x}\rfloor
$$
类似
$$
\lfloor{my}\rfloor<my<k\le\lfloor{(m+1)y}\rfloor-1<(m+1)y-1<\lfloor{(m+1)y}\rfloor
$$
取出下列不等式构成不等式组
$$
nx<k<(n+1)x-1
$$

$$
my<k<(m+1)y-1
$$

转换一下
$$
\frac{n}{k}<\frac{1}{x}<\frac{n+1}{k+1}
$$

$$
\frac{m}{k}<\frac{1}{y}<\frac{m+1}{k+1}
$$

两式相加
$$
\frac{n+m}{k}<\frac{1}{x}+\frac{1}{y}=1<\frac{n+m+2}{k+1}
$$
转换一下
$$
n+m<k<k+1<n+m+2
$$
因为n、m是正整数，所以不符合，所以$P\cup Q=Z^+$

