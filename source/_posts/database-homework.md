---
title: 数据库作业
copyright: right
mathjax: true
top: false
layout: true
date: 2019-09-24 00:30:43
categories: 课程作业
description: 记录一下数据库系统概论的作业
tags:
- database
id: gdut-database
---

## 第一次作业(19.9.24)

利用alpha语言完成以下操作

### 查询同时选修了1号课程和2号课程的学生的姓名。

`RANGE SC X`

`GET W (Student.name):`

$\exists X(X.Sno=Student.Sno \land X.Cno='1')\land \exists X(X.Sno=Student.Sno \land X.Cno='2')$

### 查询所选课程都在90分以上的学生的学号。

`RANGE SC SCX`

​             `Course CX`

`GET W (Student.Sno):`

$\forall CX \exists SXC(SXC.Grade\geq 90 \land SXC.Sno=Student.Sno \land SCX.Cno=CX.Cno)$

### 查询选修了全部“先行课为6号课” 课程的学生名字。

`RANGE SC SCX`

​             `Course CX`

`GET W (Student.Sname):`

$\forall SCX(Student.Sno=SCX.Sno\land SCX.Cno=CX.Cno \land CX.Pcno='6')$

### 四、计算题

![db1_1.png](https://cdn.jsdelivr.net/gh/denniszy/cdn/images/db1_1.png)

#### (1)

|  A   |  B   |  C   |  E   |
| :--: | :--: | :--: | :--: |
|  2   |  3   |  4   |  5   |
|  2   |  7   |  2   |  3   |

#### (2)

假装是空表，应该就是空表吧，关系都莫得

### 五、计算题

![db1_2.png](https://cdn.jsdelivr.net/gh/denniszy/cdn/images/db1_2.png)

#### (1)

|  R.B  |
| :---: |
| $b_1$ |

#### (2)

|   C   |   E   |
| :---: | :---: |
| $c_1$ | $e_1$ |
| $c_3$ | $e_3$ |

#### (3)

|   A   |   B   |   C   |   E   |
| :---: | :---: | :---: | :---: |
| $a_1$ | $b_1$ | $c_1$ | $e_2$ |
| $a_1$ | $b_2$ | $c_2$ | $e_1$ |
| $a_1$ | $b_3$ | $c_1$ | $e_1$ |
| $a_1$ | $b_3$ | $c_3$ |  $e_3$  |

#### (4)

|   C   |
| :---: |
| $c_1$ |
| $c_2$ |
| $c_3$ |

## 第二次作业(19.10.15)

### p130.4 用SQL语句建立表 

```sql
CREATE TABLE S
(

)
```

