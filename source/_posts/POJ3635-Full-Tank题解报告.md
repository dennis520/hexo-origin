---
title: '[POJ3635]Full Tank题解报告'
copyright: right
mathjax: true
top: false
categories: 题解报告
description: POJ3635题解报告
tags:
  - ACM
  - BFS
id: POJ3635
abbrlink: 976804566
date: 2019-04-22 23:04:05
---

[传送门](http://poj.org/problem?id=3635)

### 题意

给你n个点，m条边，每个点的加油站的油的价格不同，每段路要消耗d升油。给你q个询问，你的油箱最大容量为c，从s到t的最少花费。

### 题解

优先队列bfs，每个状态有两种转移的方式。

1. 如果当前状态加一升油比这个状态多一升的状态所花费更少，就转移过去，有点绕啊。看看代码8。

   ``` cpp
   if(d[u][fuel+1]>d[u][fuel]+p[u])
   	d[u][fuel+1]=d[u][fuel]+p[u];
   ```

2. 如果在这个点有能够走的路就路上的另外一个点去转移。

   ```cpp
   if(d[v][fuel-z]>d[u][fuel])
   	d[v][fuel-z]>d[u][fuel];
   ```

   

### 代码

``` cpp
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <string>
#include <cmath>
#include <ctime>
#include <queue>
#include <stack>
#include <map>
#include <set>
using namespace std;
int n,m;
int d[1010][110];
int Head[1010],nxt[20020],ver[20020],w[20020];
int p[1010];
struct node{
    int cost,u,fuel;
    node(int _cost=0,int _u=0,int _fuel=0):cost(_cost),u(_u),fuel(_fuel){};
    bool operator < (const node &x)const {
        return cost>x.cost;
    }
};
int tot;
void add(int u,int v,int d){
    ver[tot]=v;
    w[tot]=d;
    nxt[tot]=Head[u];
    Head[u]=tot++;
}
bool v[1010][110];
int bfs(int c,int s,int t){
    priority_queue<node>q;
    memset(d,0x3f,sizeof d);
    memset(v,0,sizeof v);
    q.push(node(0,s,0));
    d[s][0]=0;
    while(q.size()){
        node tmp = q.top();q.pop();
        if(tmp.u==t)return tmp.cost;
        int city = tmp.u;
        int fuel = tmp.fuel;
        if(v[city][fuel])continue;
        v[city][fuel]=1;
        if(fuel<c&&d[city][fuel+1]>d[city][fuel]+p[city]){
            d[city][fuel+1]=d[city][fuel]+p[city];
            q.push(node(d[city][fuel+1],city,fuel+1));
        }
        for(int i=Head[city],v;i;i=nxt[i]){
            v=ver[i];
            if(w[i]<=fuel&&d[v][tmp.fuel-w[i]]>d[city][tmp.fuel]){
                d[v][tmp.fuel-w[i]]=d[city][tmp.fuel];
                q.push(node(tmp.cost,v,fuel-w[i]));
            }
        }
    }
    return -1;
}
int main(){
    //freopen("in.txt","r",stdin);
    //freopen("out.txt","w",stdout);
    while(~scanf("%d%d",&n,&m)){
        tot=1;
        for(int i=0;i<n;i++)scanf("%d",p+i);
        for(int i=1,x,y,z;i<=m;i++){
            scanf("%d%d%d",&x,&y,&z);
            add(x,y,z);
            add(y,x,z);
        }
        int q;
        scanf("%d",&q);
        while(q--){
            int c,s,t;
            scanf("%d%d%d",&c,&s,&t);
            int ans=bfs(c,s,t);
            if(ans==-1){
                puts("impossible");
            }else{
                printf("%d\n",ans);
            }
        }
    }
    return 0;
}
```

### Note

在用优先队列bfs的时候，虽然维护了像普通bfs那样的有序，但是还是有点不同的。不要因为它是优先队列就猛地迈大步，还是要像普通bfs一样一步一步地走，不能迈大的步伐。