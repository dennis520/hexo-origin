---
title: Using Accesser
copyright: right
mathjax: true
top: false
layout: true
date: 2019-09-02 15:41:47
categories: 折腾笔记
description: 利用反向代理避开SNI检测
tags:
	- linux
	- Accesser
id: using-accesser
---

> Accesser是一个解决SNI RST导致维基百科、Pixiv等站点无法访问的工具

[官方repo](https://github.com/URenko/Accesser)

### SNI

Server Name Indication(SNI)是一个TLS的一个扩展，可以让一个ip拥有多个证书，也就可以让一个ip内可以架设多个通过https访问的网站。这个功能看起来与虚拟主机十分类似，但是对于https来说，原始的SNI扩展是没对这个段进行加密的，那么就是说可以被中间人所窃听的。

GFW也是利用了这个点封禁了很多网站，有更加狠的封禁就是直接封禁ip，详情看谷歌等等。

### Accesser原理

不清楚，py代码看得不太懂。就不秀智商下限了

### Accesser使用

我只能说我在manjaro下的使用了，其他linux应该也差不多的

#### 下载源码

```
git clone https://github.com/URenko/Accesser.git
```

#### 安装python

#### 安装pip3

#### 安装依赖包

```
pip3 install pyopenssl tld dnspython tornado
```

后面加`--user`就只为自己这个用户安装，否则就全局安装，哪个用户都可以用，windows可以忽略，况且windows能直接下载`.exe`文件直接跑

#### 运行

```
python3 accesser.py
```

### 添加能通过Accesser反代的网站

在`./template/pac`里面修改`var domains`的列表即可