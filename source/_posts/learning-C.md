---
title: Learning C++
copyright: right
mathjax: true
top: false
categories: 学习笔记
description: C++ primer读书笔记
tags: C++
id: cplusplus
abbrlink: 1171153983
date: 2019-05-29 14:37:09
---

# C++primer(第5版)读书笔记

## 常量

对于指针来说才会有顶层const和底层const之分

### 顶层const

顶层const指的是指针是常量，就是指针里存的地址是不可改变的。

### 底层const

底层const指的是指针所指的变量是常量，不能修改所指的常量。

### 代码

```cpp
int var1 = 1；
const int var2 = 2;
int *p1 = var1;
const int *p2 = var2;//这个指针也可以指向var1的，但是不能通过这个指针区修改所指的变量
const int *p3 const = var2;
/*
*p2和p3的区别：
*	p2能够重新指向别的变量，也就是能够改变指针的值
*	p3不能都重新指向别的变量，需要在声明的时候就必须初始化，也就是不能改变指针的值
*也就是顶层const是对于指针的修饰
*底层const是对指针权力的一个限制，就是不允许有底层const的指针修改所指变量的值。
*/
```

### constexpr(C++11)

常量表达式是用编译的时候就能知道对应表达式的值的表达式，constexpr对变量的一个修饰，要求他是一个常量，而且只能通过常量来初始化它，当然这个也可以用来修饰指针，但对于指针来说，只有是在函数体外的变量(全局变量,在堆空间里的变量)才能用constexpr来修饰，如果是在函数体内的变量(局部变量,在栈空间里的变量)只有申请之后才会知道准确的地址，也就是每次运行的时候的地址都是不确定的，这就违反了cosntexpr一定要通过常量表达式来初始化的这个规定。