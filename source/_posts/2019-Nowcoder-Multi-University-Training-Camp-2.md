---
title: 2019-Nowcoder-Multi-University-Training-Camp-2
copyright: right
mathjax: true
top: false
categories: 题解报告
description: 2019牛客多校第二场题解
tags:
  - algorithm
  - 2019-Multi-University-Training-Contest
id: 2019nowcoder2
date: 2019-07-29 01:47:27
---

## solution

### A Eddy Walker

题意：有个长度为n的环，每次从0开始，把所有格子走完的时候所在的地方为m的概率。

题解：在环上，总有点是对称的，那么在对称的点上结束的概率是一样的，如果换一个点为起点，就会找到另外的对称点，又因为不能在0点位结束，所以每个点的概率都为$\frac{1}{n-1}$。

```cpp
#include <cstdio>
#include <cstring>
using namespace std;
typedef long long ll;
const ll mod = 1000000007;
ll qpow(ll a,ll n){
    ll ans=1;
    while(n){
        if(n&1)ans=(ans*a)%mod;
        n>>=1;
        a=(a*a)%mod;
    }
    return ans;
}
int main(){
    int t;
    ll ans=1;
    scanf("%d",&t);
    while(t--){
        ll n,m;
        scanf("%lld%lld",&n,&m);
        if(n==1&&m==0){
            ans*=1;
        }else if(m==0){
            ans*=0;
        }else{
            ans*=qpow(n-1,mod-2);
            ans%=mod;
        }
        printf("%lld\n",ans);
    }
    return 0;
}
```



### B Eddy Walker2

题意：每次走1到k间的一个值，问走到n的概率，n为-1表示正无穷。

题解：设f[i]表示走到i的概率，则$f[i]$由$\frac{f[i-k-1]}{k}+...+\frac{f[i-1]}{k}$更新，设s[i]表示前缀概率和，则可写为 $f[i]=(s[i-1]-s[i-1-k])*(1/k)$,线性递推，由于数据太大，矩阵快速幂会超时，故用BM。当n为无穷时，暴力可看出，概率为$\frac{2}{k+2}$。

```cpp
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <string>
#include <vector>
#include <string>
#include <cmath>
#include <ctime>
#include <queue>
#include<stack>
#include <map>
#include <set>
#include<fstream>
#include <cassert>
#define lowbit(x) x&(-x)
using namespace std;
#define rep(i,a,n) for (int i=a;i<n;i++)
#define per(i,a,n) for (int i=n-1;i>=a;i--)
#define pb push_back
#define mp make_pair
#define all(x) (x).begin(),(x).end()
#define fi first
#define se second
#define SZ(x) ((int)(x).size())
typedef vector<int> VI;
typedef long long ll;
typedef pair<int, int> PII;
const int N = 1e4 + 5;
const int INF = 0x3f3f3f3f;
const int mod = 1e9 + 7;
ll f[N], s[N];
ll powmod(ll a, ll b) { ll res = 1; a %= mod; assert(b >= 0); for (; b; b >>= 1) { if (b & 1)res = res * a%mod; a = a * a%mod; }return res; }
// head
 
int _, n;
namespace linear_seq {
    const int N = 10010;
    ll res[N], base[N], _c[N], _md[N];
 
    vector<int> Md;
    void mul(ll *a, ll *b, int k) {
        rep(i, 0, k + k) _c[i] = 0;
        rep(i, 0, k) if (a[i]) rep(j, 0, k) _c[i + j] = (_c[i + j] + a[i] * b[j]) % mod;
        for (int i = k + k - 1; i >= k; i--) if (_c[i])
            rep(j, 0, SZ(Md)) _c[i - k + Md[j]] = (_c[i - k + Md[j]] - _c[i] * _md[Md[j]]) % mod;
        rep(i, 0, k) a[i] = _c[i];
    }
    int solve(ll n, VI a, VI b) { 
        ll ans = 0, pnt = 0;
        int k = SZ(a);
        assert(SZ(a) == SZ(b));
        rep(i, 0, k) _md[k - 1 - i] = -a[i]; _md[k] = 1;
        Md.clear();
        rep(i, 0, k) if (_md[i] != 0) Md.push_back(i);
        rep(i, 0, k) res[i] = base[i] = 0;
        res[0] = 1;
        while ((1ll << pnt) <= n) pnt++;
        for (int p = pnt; p >= 0; p--) {
            mul(res, res, k);
            if ((n >> p) & 1) {
                for (int i = k - 1; i >= 0; i--) res[i + 1] = res[i]; res[0] = 0;
                rep(j, 0, SZ(Md)) res[Md[j]] = (res[Md[j]] - res[k] * _md[Md[j]]) % mod;
            }
        }
        rep(i, 0, k) ans = (ans + res[i] * b[i]) % mod;
        if (ans < 0) ans += mod;
        return ans;
    }
    VI BM(VI s) {
        VI C(1, 1), B(1, 1);
        int L = 0, m = 1, b = 1;
        rep(n, 0, SZ(s)) {
            ll d = 0;
            rep(i, 0, L + 1) d = (d + (ll)C[i] * s[n - i]) % mod;
            if (d == 0) ++m;
            else if (2 * L <= n) {
                VI T = C;
                ll c = mod - d * powmod(b, mod - 2) % mod;
                while (SZ(C) < SZ(B) + m) C.pb(0);
                rep(i, 0, SZ(B)) C[i + m] = (C[i + m] + c * B[i]) % mod;
                L = n + 1 - L; B = T; b = d; m = 1;
            }
            else {
                ll c = mod - d * powmod(b, mod - 2) % mod;
                while (SZ(C) < SZ(B) + m) C.pb(0);
                rep(i, 0, SZ(B)) C[i + m] = (C[i + m] + c * B[i]) % mod;
                ++m;
            }
        }
        return C;
    }
    int gao(VI a, ll n) {
        VI c = BM(a);
        c.erase(c.begin());
        rep(i, 0, SZ(c)) c[i] = (mod - c[i]) % mod;
        return solve(n, c, VI(a.begin(), a.begin() + SZ(c)));
    }
}
ll qpow(ll a, ll b)
{
    ll ans = 1;
    while (b) {
        if (b & 1) {
            ans = ans * a%mod;
        }
        a = a * a%mod;
        b >>= 1;
    }
    return ans;
}
int main() {
    ll k, n;
    int t;
    scanf("%d",&t);
    while (t--) {
        scanf("%lld%lld",&k,&n);
        ll invk = qpow(k, mod - 2);
        if (n == -1) {
            printf("%lld\n",2*qpow(k+1,mod-2)%mod);
        }
        else {
            f[0] = 1, s[0] = 1;
            for (int i = 1; i < N; i++) {
                if (i <=k) {
                    f[i] = invk * s[i - 1] % mod;
                }
                else {
                    f[i] = invk * (s[i - 1] - s[i - k - 1] + mod) % mod;
                }
                s[i] = (s[i - 1] + f[i]) % mod;
            }
            vector<int>v;
            for (int i = 0; i < N; i++)
                v.push_back(f[i]);
            printf("%d\n", linear_seq::gao(v, n));
        }
    }
    return 0;
}
```



### F Partition problem

题意：给你一个n，给出$2n\times2n$的矩阵，$v_{ij}$表示$i$与$j$之间的边权，让你把$2n$个人分为两组，使得一组人与另一组人的所有的边权的和最大。

题解：枚举每种状态，每一步更新更新一次，能把$O(n^2)$的查询时间压为$O(n)$。

```cpp
#include <cstdio>
#include <unordered_map>
#include <bitset>
using namespace std;
typedef long long ll;
long long mmp[30][30];
int n,nn;
int l[20],r[20];
ll ans;
void dfs(int pos,int cnt1,int cnt2,ll res){
    if(cnt1==n&&cnt2==n){
        ans=max(ans,res);
        return;
    }
    ll tmp;
    if(cnt1<n){
        tmp=0;
        for(int i=0;i<cnt2;i++){
            tmp+=mmp[r[i]][pos];
        }
        l[cnt1]=pos;
        dfs(pos+1,cnt1+1,cnt2,res+tmp);
    }
    if(cnt2<n){
        tmp=0;
        for(int i=0;i<cnt1;i++){
            tmp+=mmp[l[i]][pos];
        }
        r[cnt2]=pos;
        dfs(pos+1,cnt1,cnt2+1,res+tmp);
    }   
    return ;
}
int main(){
    ans=0;
    scanf("%d",&n);
    nn=n<<1;
    for(int i=1;i<=nn;i++){
        for(int j=1;j<=nn;j++){
            scanf("%lld",&mmp[i][j]);
        }
    }
    l[0]=1;
    dfs(2,1,0,0);
    printf("%lld\n",ans);
    return 0;
}
```



### H Second Large Rectangle

题意：计算次大全为1的长方形的面积大小

题解：用单调栈维护全为1的长方形面积最大和次大两个值即可。 

```cpp
#include <algorithm>
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <string>
#include <cmath>
#include <ctime>
#include <queue>
#include <map>
#include <set>
/*
#include <unordered_map>
#include <unordered_set>
*/
using namespace std;
int mmp[1010][1010];
int sta[1010],w[1010],top;
int work(int x,int y) {
    if(x>y)return (x-1)*y;
    else return (y-1)*x;
}
int main() {
    //freopen("in.txt","r",stdin);
    //freopen("out.txt","w",stdout);
    int n,m;
    scanf("%d%d",&n,&m);
    for(int i=1; i<=n; i++) {
        for(int j=1; j<=m; j++) {
            scanf("%1d",&mmp[i][j]);
        }
    }
    for(int i=1; i<=n; i++) {
        for(int j=1; j<=m; j++) {
            if(mmp[i][j])mmp[i][j]+=mmp[i-1][j];
        }
    }
 
    int ans1=0,ans2=0;
    for(int i=1; i<=n; i++) {
        top=0;
        for(int j=1; j<=m+1; j++) {
            int weight=0;
            while(top&&sta[top]>mmp[i][j]) {
                if(sta[top]*(w[top]+weight)>=ans1) {
                    ans2=max(ans1,work(sta[top],(w[top]+weight)));
                    ans1=sta[top]*(w[top]+weight);
                } else if (sta[top]*(w[top]+weight)>=ans2) {
                    ans2=sta[top]*(w[top]+weight);
                }
                weight+=w[top];
                top--;
            }
            sta[++top]=mmp[i][j];
            w[top]=weight+1;
        }
    }
    printf("%d\n",ans2);
    return 0;
}
```

