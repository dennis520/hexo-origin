---
title: 自动化部署静态博客
copyright: right
mathjax: true
top: false
layout: true
date: 2019-09-26 01:11:30
categories: 折腾笔记
description: 使用Netlify/gitlab-CI/Travis-CI都可以
tags:
- hexo
- CI
id: auto-deploy-static-blog
---

# 什么是自动化部署静态博客

让别人的服务器帮你完成一定的任务，在这里就是需要完成页面生成部署。

# 为什么要这样搞？

- 方便备份，无论是配置还是文章，都能很好地进行备份

# 前情提要

- 自己先进行基本的博客框架安装
- 自行在根目录配置git

# 安装主题

通过submodule安装主题，如果用git clone，你会发现你无法把这个文件夹git上远端git服务器。

下面是安装主题`NexT`的示例代码

``` shell
git submodule add https://github.com/theme-next/hexo-theme-next.git themes/next
```

安装完之后，查看可选的release版本

``` shel
git tag
```

给这个submodule打上标记，让别人知道你需要的是哪个版本，不打这个标记可能是直接用你上传的时候最新的一个

```shell
git checkout tags/v7.4.0
```

更新子模块

``` shell
git submodule update --rebase --remote
```

hexo主题配置通过在根目录的`theme_config`来覆盖。

# 配置

## gitlab

`.gitlab-ci.yml`

``` yml
image: node:10.15.3

cache:
  paths:
    - node_modules/

variables:
  GIT_SUBMODULE_STRATEGY: recursive

before_script:
  - npm install hexo-cli -g
  - test -e package.json && npm install
  - hexo generate

pages:
  script:
    - hexo generate
  artifacts:
    paths:
      - public
  only:
    - master
```

## netlify

`netlify.toml`

``` toml
[build]
  command = "npm run build"
  publish = "/public"
```

