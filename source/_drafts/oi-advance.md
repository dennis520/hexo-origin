---
title: oi-advance
copyright: right
mathjax: true
top: false
layout: true
date: 2019-08-27 00:56:15
categories: 读书计划
description: 记录题目的简略的题意、题解和代码
tags: 
	- ACM
	- algorithm
id: oi-advance
---

书籍:算法竞赛进阶指南第三版

## 0x00 基本算法

### 0x01 位运算

#### a^b(CH0101)

##### 题意

求a的b次方对p取模的值。

##### 题解

通过对b划分为$\lceil\log_2{(b+1)}\rceil$个二进制位，利用模运算的定理求出$a^b\pmod {p}$

```cpp
typedef long long ll;
ll qpow(ll a,ll b,ll p){
    ll ans = 1%p;
	for(; b; b >>= 1){
        if(b & 1)(ans *= a) %= p;
        (a *= a) %= p;
    }
    return ans;
}
```

#### 64位整数乘法(CH0102)

```cpp
typedef long long ll;
long long multi(long long x,long long y,long long mod)
{
    long long ans=0;
    x%=mod,y%=mod;
    while(y){
        if(y&1){
            ans=(ans+x)%mod;   
        }
        y>>=1;
        (x<<=1)%=mod;
    }
    return ans;
}
```



### 0x02 递推与递归

### 0x03

